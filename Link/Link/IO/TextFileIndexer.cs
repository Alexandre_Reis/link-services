﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Link.IO
{
    /// <summary>
    /// A utility class for indexing delimited text files
    /// </summary>
    public class TextFileIndexer
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="TextFileIndexer" /> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="colSeperator">The col seperator.</param>
        /// <param name="rowSeperator">The row seperator.</param>
        /// <param name="hasHeader">The has header.</param>
        /// <param name="valueHandler">The value handler.</param>
        public TextFileIndexer(ref String text, String colSeperator, String rowSeperator, Boolean hasHeader, Func<Int32, Int32, String> valueHandler)
        {
            if ((rowSeperator == "CRLF") || (rowSeperator == "\\r\\n") || (rowSeperator == "\\r") || (rowSeperator == "\r\n") || (rowSeperator == "\r"))
            {
                if (text.IndexOf("\r\n") >= 0)
                    rowSeperator = "\r\n";
                else if (text.IndexOf("\n") >= 0)
                    rowSeperator = "\n";
                else
                    rowSeperator = "\r";
            }

            this.FieldNames = new List<TextFileField>();
            this.Rows = new TextFileRows();

            TextFileRow cols = new TextFileRow();

            Char[] colSepChars = colSeperator.ToArray();
            Char[] rowSepChars = rowSeperator.ToArray();
            Char curChar = ' ';

            Int32 cellSta = 0;
            Int32 cellLen = 0;
            Int32 charIdx = 0;

            while (charIdx < text.Length)
            {
                curChar = text[charIdx];

                // process column separator characters
                if (colSepChars[0] == curChar)
                {
                    // check for valid character sequence
                    if ((colSepChars.Length == 1) || ValidSequence(ref text, colSepChars, charIdx))
                    {
                        TextFileField cell = new TextFileField(cellSta, cellLen, valueHandler);
                        cols.Add(cell);

                        charIdx += colSepChars.Length;
                        cellSta = charIdx;
                        cellLen = 0;
                    }
                    // not a valid character sequence
                    else
                    {
                        charIdx += 1;
                        cellLen += 1;
                    }
                }

                // process row separator characters
                else if (rowSepChars[0] == curChar)
                {
                    // check for valid character sequence
                    if ((rowSepChars.Length == 1) || ValidSequence(ref text, rowSepChars, charIdx))
                    {
                        TextFileField cell = new TextFileField(cellSta, cellLen, valueHandler);
                        cols.Add(cell);

                        if (hasHeader && this.FieldNames.Count == 0)
                            this.FieldNames.AddRange(cols);
                        else
                            this.Rows.Add(cols);

                        cols = new TextFileRow();
                        charIdx += rowSepChars.Length;
                        cellSta = charIdx;
                        cellLen = 0;
                    }
                    // not a valid character sequence
                    else
                    {
                        charIdx += 1;
                        cellLen += 1;
                    }
                }

                // process all other characters
                else
                {
                    charIdx += 1;
                    cellLen += 1;
                }
            }

            // check for last row/col
            if (cellLen > 0)
            {
                TextFileField cell = new TextFileField(cellSta, cellLen, valueHandler);
                cols.Add(cell);

                if (hasHeader && this.FieldNames.Count == 0)
                    this.FieldNames.AddRange(cols);
                else
                    this.Rows.Add(cols);
            }
        }
        private Boolean ValidSequence(ref String text, Char[] sequence, Int32 txtIdx)
        {
            if (txtIdx + sequence.Length >= text.Length)
                return false;

            for (int seqIdx = 0; seqIdx < sequence.Length; seqIdx++)
            {
                if (sequence[seqIdx] != text[txtIdx])
                    return false;

                txtIdx++;
            }

            return true;
        }


        #endregion

        #region Properties

        public List<TextFileField> FieldNames { get; private set; }

        /// <summary>
        /// Gets the Rows Index;
        /// </summary>
        /// <value>
        /// The rows.
        /// </value>
        public TextFileRows Rows { get; private set; }

        #endregion
    }
}
