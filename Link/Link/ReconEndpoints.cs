﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DevExpress.Spreadsheet;
using Link.Client;
using Link.Core;
using Link.Entity.Model;
using Link.Model;
using Link.ViewModel;

namespace Link
{
    public class ReconEndpoints
    {
        #region constructor

        private static Common.Logging.ILog _logger;

        public ReconEndpoints(string hostName, Int32 hostPort, Boolean isEncrypted, Int32 failOverRetryMins, Int32 defaultServerConnectionTimeout, Int32 extendedServerConnectionTimeout, string logFile, string xlsFolder)
        {
            AppSettings.Settings.Instance().HostName = hostName;
            AppSettings.Settings.Instance().HostPort = hostPort;
            AppSettings.Settings.Instance().IsEncrypted = isEncrypted;
            AppSettings.Settings.Instance().FailOverRetryMins = failOverRetryMins;
            AppSettings.Settings.Instance().DefaultServerConnectionTimeout = defaultServerConnectionTimeout;
            AppSettings.Settings.Instance().ExtendedServerConnectionTimeout = extendedServerConnectionTimeout;
            AppSettings.Settings.Instance().LogDir = logFile;
            AppSettings.Settings.Instance().XlsFolder = xlsFolder;

            // initialize the entry-point logger with a SafeLogger to ensure fatal exceptions are logged
            _logger = new Common.Logging.Simple.ConsoleOutLogger("Console", Common.Logging.LogLevel.All, true, true, false, "G");

            //Logger = new Common.Logging.Simple.ConsoleOutLogger("Console", Common.Logging.LogLevel.All, true, true, false, "G");
            ServiceLocator.Register(typeof (Common.Logging.ILoggerFactoryAdapter), typeof (Logging.AtlasNLogFactory));

            //IMPORTANT: initialize cross-cutting concerns
            _logger = ServiceLocator.Get<Common.Logging.ILoggerFactoryAdapter>().GetLogger(typeof (ReconEndpoints)); // if this fails, the SafeLogger is still enabled

            ServiceLocator.Register(typeof (Net.IConnectionProvider), typeof (Net.ConfigConnectionProvider));
            _logger.Info(string.Format("ReconEndpoints has been created. hostName: {0}, hostPort: {1}, isEncrypted: {2}, failOverRetryMins: {3}, defaultServerConnectionTimeout: {4}, extendedServerConnectionTimeout: {5}, logFile: {6}", hostName, hostPort, isEncrypted, failOverRetryMins, defaultServerConnectionTimeout, extendedServerConnectionTimeout, logFile));

        }

        #endregion

        #region public Methods

        public HeartbeatMessage Heartbeat()
        {
            var client = new HeartbeatClient();
            var result = client.GetHeartbeat();
            return result;
        }

        public ReconciliationResultResponse GetReconResultPosition(string messageId)
        {
            try
            {
                _logger.Info("GetReconResultPosition");

                _logger.Debug(string.Format("Getting the recon result for {0}",messageId));

                var clientDetail = new TransportReconciliationMessageClient();
                ReconciliationResult entity = clientDetail.GetDetail(messageId);
                
                if (entity == null)
                {
                    return new ReconciliationResultResponse
                           {
                        Response = new LinkResponse
                        {
                            ResponseStatus = ResponseType.ERROR,
                            Text = string.Format("Recon result not found for {0}",messageId)
                        }
                    };
                }

                _logger.Debug("Getting the recon route position");
                var route = ReconRoutePosition();
                if (route == null)
                {
                    return new ReconciliationResultResponse
                           {
                               Response = new LinkResponse
                                          {
                                              ResponseStatus = ResponseType.ERROR,
                                              Text = "Recon route not found"
                                          }
                           };
                }

                var viewmodel = new ReconResultViewModel(entity, route);

                var workbook = new Workbook();
                GenerateWorkSheet(workbook, "Linhas Combinadas", viewmodel.MatchedRows);
                GenerateWorkSheet(workbook, "Linhas Combinaram com Var", viewmodel.MatchedVarRows);
                GenerateWorkSheet(workbook, "Linhas Divergentes", viewmodel.UnmatchedRows);
                GenerateWorkSheet(workbook, "Esquerda não a Direita", viewmodel.LeftNotInRightRows);
                GenerateWorkSheet(workbook, "Direita não a Esquerda", viewmodel.RightNotInLeftRows);
                GenerateWorkSheet(workbook, "Linhas Esquerda duplicadas", viewmodel.LeftDuplicateRows);
                GenerateWorkSheet(workbook, "Linhas Duplicadas Direita", viewmodel.RightDuplicateRows);

                workbook.Worksheets.RemoveAt(0);

                var xlsFileName = string.Format("ReconResult_{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmssfff"));
                var xlsFilePath = AppSettings.Settings.Instance().XlsFolder;
                var xlfFile = Path.Combine(xlsFilePath, xlsFileName);

                workbook.SaveDocument(xlfFile, DocumentFormat.OpenXml);

                //var response = new ReconciliationResultResponse
                //{
                //    XlsFile = xlfFile,
                //    Response = new LinkResponse
                //    {
                //        ResponseStatus = ResponseType.OK,
                //        Text = ""
                //    }
                //};

                ReconciliationResultResponse response;

                using (var stream = new FileStream(xlfFile, FileMode.Open, FileAccess.Read))
                {
                    //workbook.SaveDocument(stream, DocumentFormat.OpenXml);

                    //byte[] buffer = new byte[stream.Length];
                    //using (MemoryStream ms = new MemoryStream())
                    //{
                    //    int read;
                    //    while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                    //    {
                    //        ms.Write(buffer, 0, read);
                    //    }
                    //    var byteArray = ms.ToArray();

                    //    streamString = Convert.ToBase64String(byteArray);
                    //}

                    response = new ReconciliationResultResponse
                    {
                        XlsFileName = xlsFileName,
                        XlsFilePath = xlsFilePath,
                        Response = new LinkResponse
                        {
                            ResponseStatus = ResponseType.OK,
                            Text = ""
                        }
                    };
                }

                _logger.Info("GetReconMessagePosition, null message");
                return response;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return new ReconciliationResultResponse
                {
                    Response = new LinkResponse
                    {
                        ResponseStatus = ResponseType.ERROR,
                        Text = ex.Message
                    }
                };
            }
        }

        public void Logoff()
        {
            var client = new LogonClient(AppSettings.Settings.Instance().ExtendedServerConnectionTimeout);
            client.Logout();
        }

        public LoginResult Authenticate(string user, string password)
        {
            try
            {
                _logger.Info(string.Format("Authenticate user:{0}, password:{1}", user, password));

                var request = new LogonRequest { UserName = user, Password = password };
                var client = new LogonClient(AppSettings.Settings.Instance().ExtendedServerConnectionTimeout);
                var result = client.LogonUser(request);
                if (result != null)
                {
                    _logger.Info(string.Format("Authenticate Message:{0}, Status:{1}", result.Status, result.Message));
                    var response = new LinkResponse
                                   {
                                       ResponseStatus = ResponseType.OK, Text = result.Message
                                   };

                    return new LoginResult
                           {
                               Response = response,
                               Message = result.Message,
                               Status = result.Status
                           };
                }

                _logger.Info("Authenticate null result");
                return null;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

                return new LoginResult
                {
                    Response = new LinkResponse
                    {
                        ResponseStatus = ResponseType.ERROR,
                        Text = string.Format("{0} {1}", ex.Message, ex.InnerException.Message)
                    },
                    Message = "", 
                    Status = LogonStatus.ERROR
                };
            }
            
        }

        public ReconRouteFileLocation GetReconRoutePosition()
        {
            try
            {
                _logger.Info("GetReconRoutePosition");
                var ret = new ReconRouteFileLocation();
                var route = ReconRoutePosition();
                if (route == null)
                {
                    _logger.Info("GetReconRoutePosition, route not found");
                    return ret;
                }

                ret.RouteId = route.ID;
                ret.SourceFileAname = GetEndpointAttribute(route.LeftInEndpoint, "fileName");
                ret.SourceFileApath = GetEndpointAttribute(route.LeftInEndpoint, "fileLocation");

                ret.SourceFileBname = GetEndpointAttribute(route.RightInEndpoint, "fileName");
                ret.SourceFileBpath = GetEndpointAttribute(route.RightInEndpoint, "fileLocation");

                ret.OutputPath = GetEndpointAttribute(route.OutEndpoints.FirstOrDefault(), "fileLocation");

                _logger.Info(string.Format("RouteId:{0}, SourceFileApath:{1}, SourceFileAname:{2}, SourceFileBpath:{3}, SourceFileBname:{4}, OutputPath:{5}", ret.RouteId, ret.SourceFileApath, ret.SourceFileAname, ret.SourceFileBpath, ret.SourceFileBname, ret.OutputPath));

                return ret;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);

                return new ReconRouteFileLocation
                       {
                           Response = new LinkResponse
                                      {
                                          ResponseStatus = ResponseType.ERROR,
                                          Text = ex.Message
                                      }
                       };
            }
            
        }

        public ReconciliationMessageResponse GetReconMessagePosition()
        {
            try
            {
                _logger.Info("GetReconMessagePosition");

                var route = ReconRoutePosition();
                if (route == null)
                    return null;

                var dt = DateTime.Today;

                var client = new TransportReconciliationMessageClient();
                var messages = client.GetMessagesFrom(dt);
                if (messages != null)
                {
                    var messageList = messages.Where(x => x.ReconciliationRoute.ID == route.ID).Select(message => new ReconciliationMessage
                    {
                        CreatedAt = message.CreatedAt,
                        UpdatedAt = message.UpdatedAt,
                        RouteId = message.ReconciliationRoute.ID,
                        MessageStatus = message.MessageStatus,
                        LastDuration = message.LastDuration,
                        MessageId = message.ID
                    }).ToList();

                    _logger.Info(string.Format("GetReconMessagePosition, {0} messages returned", messageList.Count));

                    var response = new ReconciliationMessageResponse
                                   {
                                       Response = new LinkResponse
                                                  {
                                                      ResponseStatus = ResponseType.OK
                                                  },
                                       Messages = messageList
                                   };


                    return response;
                }

                _logger.Info("GetReconMessagePosition, null message");
                return null;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return new ReconciliationMessageResponse
                {
                    Response = new LinkResponse
                    {
                        ResponseStatus = ResponseType.ERROR,
                        Text = ex.Message
                    }
                };
            }
            
        }

        public LinkResponse TriggerReconPosition()
        {
            try
            {
                _logger.Info("TriggerReconPosition");

                var route = ReconRoutePosition();
                if (route == null)
                    return null;

                var heartbeat = Heartbeat();

                _logger.Info(string.Format("TriggerReconPosition, recon route id: {0}", route.ID));

                var client = new ReconciliationRouteClient();
                var ret = new LinkResponse();
                var response = client.Trigger(route.ID);

                if (response == null)
                {
                    response = new StandardResponse();
                }

                _logger.Info(string.Format("TriggerReconPosition, result: {0}, {1}", response.ResponseStatus, response.Text));
                ret.ResponseStatus = response.ResponseStatus;
                ret.Text = response.Text;
                ret.ServerDate = heartbeat == null ? DateTime.Now : heartbeat.ServerDate;

                return ret;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw;
            }
            
        }

        #endregion

        #region Private Methods

        private ReconciliationRoute ReconRoutePosition()
        {
            _logger.Debug("ReconRoutePosition...");
            var result = GetReconRoutes().FirstOrDefault(x => x.Category == "XMLPOSICAOANBIMA");

            if(result==null)
                _logger.Debug("Recon route position not found. Verify please the recon route with category is XMLPOSICAOAMBIMA");

            return result;
        }

// ReSharper disable once UnusedMember.Local
        private RouteList GetRoutes()
        {
            var list = new RouteClient().GetAllRoutes();
            return list;
        }

        private ReconciliationRouteList GetReconRoutes()
        {
            var list = new ReconciliationRouteClient().GetAllRoutes();
            return list;
        }

        private string GetEndpointAttribute(Endpoint endpoint, string attribute)
        {
            try
            {
                var attributeList = EndpointAttributeList.Deserialize(endpoint.Attributes);

                if (attributeList != null)
                {
                    var firstOrDefault = attributeList.FirstOrDefault(x => x.Name == attribute);
                    if (firstOrDefault != null)
                        return firstOrDefault.Value;

                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw;
            }
            
        }

        private void GenerateWorkSheet( Workbook workbook, string description, object rows)
        {
            var worksheet = workbook.Worksheets.Add();
            worksheet.Name = description;

            List<DynamicFieldDescriptor> fieldDescriptors;
            BaseDynamicViewModel reconRows;

            if (rows is ReconResultDiffDataViewModel)
            {
                fieldDescriptors = (rows as ReconResultDiffDataViewModel).MetaData.FieldDescriptors;
                reconRows = (rows as ReconResultDiffDataViewModel);
            }
            else
            {
                fieldDescriptors = (rows as ReconResultErrorDataViewModel).MetaData.FieldDescriptors;
                reconRows = (rows as ReconResultErrorDataViewModel);
            }


            // header
            int i = 0;
            foreach (var dynamicFieldDescriptor in fieldDescriptors)
            {
                worksheet[0, i].Value = dynamicFieldDescriptor.DisplayName;
                worksheet[0, i].Font.FontStyle = SpreadsheetFontStyle.Bold;
                worksheet[0, i].FillColor = System.Drawing.Color.Khaki;
                worksheet.Columns[i].Width = 400;
                i++;
            }

            // data
            i = 1;
            foreach (var matchedRow in reconRows)
            {
                var j = 0;
                foreach (var dynamicFieldDescriptor in fieldDescriptors)
                {
                    worksheet[i, j].Value = matchedRow.GetFieldValue(dynamicFieldDescriptor.Name).ToString();
                    j++;
                }
                i++;
            }
        }

        #endregion
    }
}
