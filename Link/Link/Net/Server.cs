﻿using System;
using System.Net;
using System.Linq;
using System.Diagnostics.Contracts;
using Link.Core;

namespace Link.Net
{
    /// <summary>
    /// Defines a network server.
    /// </summary>
    public class Server : IServer
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="Server" /> class.
        /// </summary>
        /// <param name="host">The host name or address.</param>
        /// <param name="port">The port number.</param>
        /// <param name="isEncrypted">The flag that determines whether the channel is encrypted.</param>
        public Server(String host, Int32 port, Boolean isEncrypted)
        {
            String hostName = host.Trim().ToLower();
            String hostAddr = hostName;
            Boolean isIPv6 = false;

            Contract.Requires(String.IsNullOrWhiteSpace(hostName) == false, "The host parameter is mandatory");
            Contract.Requires(port > 1024, "The port parameter may not be less than 1024");

            // use DNS to resolve the IP address for remote host-names
            if (Constants.LocalHostAliases.DoesNotContain(hostName))
            {
                IPAddress[] addresses = Dns.GetHostAddresses(host); // resolve available IP addresses

                if ((addresses == null) || (addresses.Length == 0))
                    throw new Exception(String.Format("DNS was unable to resolve the host [{0}], or the host is an IPv6 address and the client does not support it", hostName));

                hostAddr = addresses[0].ToString(); // use the first address
                isIPv6 = (addresses[0].AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6);

                // if the DNS lookup found a "hosts" file entry that points to localhost then use the hostname not the address
                if (Constants.LocalHostAddresses.Contains(hostAddr))
                    hostAddr = hostName;
            }

            this.HostName = hostName;
            this.HostPort = port;
            this.IsEncrypted = isEncrypted;
            this.HostAddress = hostAddr;
            this.IsIPv6 = isIPv6;

            _uri = SocketUtils.BuildHttpUrl(this.HostAddress, this.HostPort, this.IsEncrypted);
            _isActive = true; // SocketUtils.IsTcpServerListening(host, port);
            _lastConnected = DateTime.MinValue;
        }

        #endregion

        #region Fields

        private Object _syncObject = new Object(); // thread lock object

        #endregion

        #region Properties

        /// <summary>
        /// Gets the name of the host.
        /// </summary>
        /// <value>
        /// The name of the host. This could be a machine name and not an address.
        /// </value>
        public String HostName { get; private set; }

        /// <summary>
        /// Gets the address of the host.
        /// </summary>
        /// <value>
        /// The address of the host. This could be an IPv4 or IPv6 address.
        /// </value>
        public String HostAddress { get; private set; }

        /// <summary>
        /// Gets the host port.
        /// </summary>
        /// <value>
        /// The host port.
        /// </value>
        public Int32 HostPort { get; private set; }

        /// <summary>
        /// Gets a value that determines whether this server connection is encrypted.
        /// </summary>
        /// <value>
        /// The encryption flag value.
        /// </value>
        public Boolean IsEncrypted { get; private set; }

        /// <summary>
        /// Gets the is ip v6.
        /// </summary>
        /// <value>
        /// The is ip v6.
        /// </value>
        public Boolean IsIPv6 { get; private set; }

        #endregion

        #region IServer

        /// <summary>
        /// Gets the URI.
        /// </summary>
        /// <value>
        /// The URI.
        /// </value>
        public Uri URI
        {
            get { return _uri; }
        }
        private Uri _uri;

        /// <summary>
        /// Gets or sets whether the server is active.
        /// </summary>
        /// <value>
        /// The active status.
        /// </value>
        public Boolean IsActive
        {
            get
            {
                Boolean result;
                lock (_syncObject)
                {
                    result = _isActive;
                }
                return result;
            }
            set
            {
                lock (_syncObject)
                {
                    _isActive = value;
                }
            }
        }
        private Boolean _isActive;

        /// <summary>
        /// Gets or sets the last connected time.
        /// </summary>
        /// <value>
        /// The last connected time.
        /// </value>
        public DateTime LastConnected
        {
            get
            {
                DateTime result;
                lock (_syncObject)
                {
                    result = _lastConnected;
                }
                return result;
            }
            set
            {
                lock (_syncObject)
                {
                    _lastConnected = value;
                }
            }
        }
        private DateTime _lastConnected;

        /// <summary>
        /// Updates the LastConnected value with the current timestamp.
        /// </summary>
        public void UpdatedLastConnected()
        {
            this.LastConnected = DateTime.Now;
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.URI.ToString();
        }
        public override int GetHashCode()
        {
            return this.URI.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return obj != null &&
                   obj.GetType() == this.GetType() &&
                   obj.GetHashCode() == this.GetHashCode();
        }

        #endregion
    }
}
