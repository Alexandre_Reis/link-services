﻿using System;

namespace Link.Net
{
    /// <summary>
    /// Defines constants for network settings.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// The default server connection timeout in milliseconds.
        /// </summary>
        //public static readonly Int32 DefaultServerConnectionTimeout = 20000;  // 20 seconds
        /// <summary>
        /// The extended server connection timeout in milliseconds.
        /// </summary>
        //public static readonly Int32 ExtendedServerConnectionTimeout = 40000; // 40 seconds

        /// <summary>
        /// An array of IP addresses that represent localhost.
        /// </summary>
        public static readonly String[] LocalHostAddresses = new String[] { "127.0.0.1" };             //TODO: IPv6
        /// <summary>
        /// An array of host names that represent localhost.
        /// </summary>
        public static readonly String[] LocalHostAliases = new String[] { "localhost", "localhost." }; // localhost. for Fiddler

        /// <summary>
        /// The default network service name.
        /// </summary>
        public static readonly String DefaultNetworkServiceName = ".";

        /// <summary>
        /// The key name for REST sessions.
        /// </summary>
        public static readonly String SessionKey = "JSESSIONID";
    }
}
