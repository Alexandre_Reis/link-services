﻿using System;

namespace Link.Net
{
    /// <summary>
    /// Exceptions raised by the Rest CLient
    /// </summary>
    public class RestClientException : Exception
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="RestClientException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public RestClientException(string message) : base(message) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RestClientException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public RestClientException(string message, Exception innerException) : base(message, innerException) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RestClientException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public RestClientException(string message, params Object[] args) : base(String.Format(message, args)) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RestClientException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        /// <param name="args">The args.</param>
        public RestClientException(string message, Exception innerException, params Object[] args) : base(String.Format(message, args), innerException) { }

        #endregion
    }
}
