﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Link.Net
{
    public interface IServiceDescriptor<TService>
    {
        TService GetService();
    }
}
