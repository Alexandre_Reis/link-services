﻿using System;

namespace Link.Net
{
    public interface IServer
    {
        Uri URI { get; }
        Boolean IsActive { get; }
        DateTime LastConnected { get; }

        void UpdatedLastConnected();
    }
}
