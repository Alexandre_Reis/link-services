﻿using System;
using System.Collections.Generic;
using System.Linq;
using Link.AppSettings;
using Link.Core;

namespace Link.Net
{
    /// <summary>
    /// Provides connection information for clients
    /// </summary>
    public class ConfigConnectionProvider : IConnectionProvider
    {
        private static Common.Logging.ILog Logger = Common.Logging.LogManager.GetLogger(typeof(ConfigConnectionProvider));

        private static String GetKeyName(String serviceName)
        {
            if (String.IsNullOrWhiteSpace(serviceName))
                return Constants.DefaultNetworkServiceName;
            else
                return serviceName;
        }

        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigConnectionProvider" /> class.
        /// </summary>
        /// <exception cref="System.Exception"></exception>
        public ConfigConnectionProvider()
        {
            _serviceList = new Dictionary<String, List<Server>>();
            _settings = new ApplicationConfig
                        {
                            HostName = Settings.Instance().HostName,
                            HostPort = Settings.Instance().HostPort,
                            DefaultServerConnectionTimeout = Settings.Instance().DefaultServerConnectionTimeout,
                            ExtendedServerConnectionTimeout = Settings.Instance().ExtendedServerConnectionTimeout,
                            IsEncrypted = Settings.Instance().IsEncrypted,
                            FailOverRetryMins = Settings.Instance().FailOverRetryMins
                        };
            _primaryHost = new Server(_settings.HostName, _settings.HostPort, _settings.IsEncrypted);

            if (SocketUtils.IsTcpServerListening(_primaryHost.HostAddress, _primaryHost.HostPort) == false)
                throw new Exception(string.Format("Primary host is not available. Host={0}", _primaryHost.URI));

            _serviceList.Add(Constants.DefaultNetworkServiceName, new List<Server> { _primaryHost });

            Logger.TraceFormat("Primary host = {0}", _primaryHost.URI);
            Logger.TraceFormat("Load balancing enabled = {0}", _settings.ClientLoadBalancingEnabled);

#if DEBUG
            StartNetworkListener();
#endif
        }

        #endregion

        #region Fields

        private Server _primaryHost;
        private Dictionary<String, List<Server>> _serviceList;
        private ApplicationConfig _settings;

        #endregion

        #region IConnectionProvider Members

        /// <summary>
        /// Gets a list of available servers.
        /// This is the method that implements load-balancing for the REST client.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <returns>
        /// The list of available servers ordered by priority or load-balancing sequence.
        /// </returns>
        //[System.Diagnostics.DebuggerStepThrough()]
        public IList<IServer> GetServers(String serviceName)
        {
            List<IServer> result = new List<IServer>();

            lock (_serviceList)
            {
                String keyName = GetKeyName(serviceName);
                List<Server> serverList;

                if (_serviceList.TryGetValue(keyName, out serverList))
                {
                    if (_settings.ClientLoadBalancingEnabled)
                        result = serverList.OrderBy(x => x.LastConnected).Cast<IServer>().ToList();           // order by least active (least recently connected to)
                    else
                        result = serverList.OrderByDescending(x => x.LastConnected).Cast<IServer>().ToList(); // order by most active (most recently connected to)
                }
                else if (keyName != Constants.DefaultNetworkServiceName)
                {
                    serverList = new List<Server> { _primaryHost };
                    _serviceList.Add(keyName, serverList);
                    result.Add(_primaryHost);

                    Logger.TraceFormat("Service auto-registered: Service={0}, Host={1}", keyName, _primaryHost.URI.ToString());
                }
            }

            return result;
        }
        /// <summary>
        /// Registers the server.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="hostName">Name of the host.</param>
        public void RegisterServer(String serviceName, String hostName)
        {
            RegisterServer(serviceName, hostName, _primaryHost.HostPort); // use the default server port
        }
        /// <summary>
        /// Registers the server.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="hostName">Name of the host.</param>
        /// <param name="hostPort">The host port.</param>
        public void RegisterServer(String serviceName, String hostName, Int32 hostPort)
        {
            Server server = new Server(hostName, hostPort, _primaryHost.IsEncrypted);

            if (SocketUtils.IsTcpServerListening(server.HostAddress, server.HostPort) == false)
            {
                Logger.ErrorFormat("RegisterServer: requested standby server [{0}] is unreachable", server.URI);
                return;
            }

            lock (_serviceList)
            {
                String keyName = GetKeyName(serviceName);
                List<Server> serverList;

                if (_serviceList.TryGetValue(keyName, out serverList) == false)
                {
                    serverList = new List<Server>();
                    _serviceList.Add(keyName, serverList);
                }

                if (serverList.Contains(server) == false)
                {
                    serverList.Add(server);
                    Logger.TraceFormat("Server registered: Service={0}, Host={1}", keyName, server.URI.ToString());
                }
            }
        }
        /// <summary>
        /// Unregisters the server.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="hostName">Name of the host.</param>
        public void UnregisterServer(String serviceName, String hostName)
        {
            UnregisterServer(serviceName, hostName, _primaryHost.HostPort); // use the default server port
        }
        /// <summary>
        /// Unregisters the server.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="hostName">Name of the host.</param>
        /// <param name="hostPort">The host port.</param>
        /// <exception cref="System.Exception">UnregisterServer: invalid request to un-register primary server</exception>
        public void UnregisterServer(String serviceName, String hostName, Int32 hostPort)
        {
            Server server = new Server(hostName, hostPort, _primaryHost.IsEncrypted);

            lock (_serviceList)
            {
                String keyName = GetKeyName(serviceName);
                List<Server> serverList;

                if (_serviceList.TryGetValue(keyName, out serverList))
                {
                    Int32 index = serverList.IndexWhere(x => x.Equals(server));

                    if (index == 0)
                        throw new Exception("UnregisterServer: invalid request to un-register primary server for service " + keyName);

                    if (index > 0)
                    {
                        serverList[index].IsActive = false; //NB: tell all client with access to this instance to ignore it
                        serverList.RemoveAt(index);         // remove from registered server list

                        Logger.TraceFormat("Server unregistered: Service={0}, Host={1}", keyName, server.URI.ToString());
                    }
                }
            }
        }

        #endregion

        #region Utility Members

        private void StartNetworkListener()
        {
            
        }

        #endregion
    }
}
