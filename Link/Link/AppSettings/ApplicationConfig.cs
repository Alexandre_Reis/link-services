﻿using System;
using System.Xml.Serialization;

namespace Link.AppSettings
{
    [Serializable]
    [XmlRoot("application")]
    public class ApplicationConfig 
    {
        private Int32 _pollingReadDelay;
        private Int32 _defaultServerConnectionTimeout;
        private Int32 _extendedServerConnectionTimeout;

        [XmlElement("network_host_name")]
        public String HostName { get; set; }

        [XmlElement("network_host_port")]
        public Int32 HostPort { get; set; }

        [XmlElement("network_encrypted")]
        public Boolean IsEncrypted { get; set; }

        [XmlArray("network_service_hosts")]
        [XmlArrayItem("network_service_host")]

        [XmlElement("network_polling_read_delay")]
        public Int32 PollingReadDelay
        {
            get
            {
                if (_pollingReadDelay == 0 || _pollingReadDelay == null)
                    return 2000;
                else
                    return _pollingReadDelay;
            }

            set { _pollingReadDelay = value; }
        }

        [XmlElement("network_failover_retry_mins")]
        public Int32 FailOverRetryMins { get; set; }

        [XmlElement("default_server_connection_timeout")]
        public Int32 DefaultServerConnectionTimeout
        {
            get
            {
                if (_defaultServerConnectionTimeout == 0 || _defaultServerConnectionTimeout == null)
                    return 20000;
                else
                    return _defaultServerConnectionTimeout;
            }

            set { _defaultServerConnectionTimeout = value; }
        }

        [XmlElement("extended_server_connection_timeout")]
        public Int32 ExtendedServerConnectionTimeout
        {
            get
            {
                if (_extendedServerConnectionTimeout == 0 || _extendedServerConnectionTimeout == null)
                    return 40000;
                else
                    return _extendedServerConnectionTimeout;
            }

            set { _extendedServerConnectionTimeout = value; }
        }

        [XmlElement("network_client_loadbalance_enabled")]
        public Boolean ClientLoadBalancingEnabled { get; set; }

        [XmlElement("user_interface_locale")]
        public String UILocale { get; set; }

        [XmlElement("user_interface_theme")]
        public String UITheme { get; set; }

        [XmlElement("log_dir")]
        public String LogDir { get; set; }
    }
}
