﻿
using System;
using System.Collections.Generic;
using Link.Client;
using Link.Entity.Model;

namespace Link.AppSettings
{
    public class Settings
    {
        #region fields

        private static Settings _settings;

        #endregion

        #region constructor

        private Settings()
        {

        }

        public static Settings Instance()
        {
            if (_settings == null)
            {
                _settings = new Settings();
            }
            return _settings;
        }

        #endregion

        #region properties
        public String HostName { get; set; }
        public Int32 HostPort { get; set; }
        public Boolean IsEncrypted { get; set; }
        public Int32 PollingReadDelay{ get; set; }
        public Int32 FailOverRetryMins { get; set; }
        public Int32 DefaultServerConnectionTimeout{ get; set; }
        public Int32 ExtendedServerConnectionTimeout { get; set; }
        public string LogDir { get; set; }
        public string XlsFolder { get; set; }
        #endregion

        #region REST Clients

        private EndpointClient _endpointClient;

        #endregion

        #region Cached Data

        private IList<EndpointAttributeDefinition> _endpointAttributeDefinition;

        #endregion

        public IList<EndpointAttributeDefinition> GetEndpointAttributeDefinitions()
        {
            lock (_endpointClient)
            {
                if (_endpointAttributeDefinition == null)
                {
                    _endpointAttributeDefinition = _endpointClient.GetMetaData();
                }
            }

            return _endpointAttributeDefinition;
        }
    }
}
