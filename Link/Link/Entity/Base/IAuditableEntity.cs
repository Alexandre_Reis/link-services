﻿using System;

namespace Link.Entity.Base
{
    /// <summary>
    /// Defines an Auditable Model Entity.
    /// </summary>
    public interface IAuditableEntity : IEntity
    {

        DateTime CreatedAt { get; }

        DateTime UpdatedAt { get; }

        Boolean? Deleted { get; }
    }
}
