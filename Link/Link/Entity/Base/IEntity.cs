﻿using System;

namespace Link.Entity.Base
{
    /// <summary>
    /// Defines a Model Entity.
    /// </summary>
    public interface IEntity
    {
        String ID { get; }
    }
}
