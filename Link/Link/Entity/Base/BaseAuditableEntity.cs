﻿using System;
using System.Xml.Serialization;

namespace Link.Entity.Base
{
    /// <summary>
    /// A base class for entities.
    /// </summary>
    [Serializable]
    public abstract class BaseEntity : IEntity
    {
        // global reference counter for new entity id's
        private static Int32 GlobalNewRowHasCode = 0;

        #region Serializable Properties

        [XmlElement(ElementName = "id")]
        public virtual String ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = (value != null) ? value : "";
            }
        }
        private String _id = ""; // set by new(), but not during deserialization

        #endregion

        #region overrides

        private Int32 _newRowHashCode = 0;

        public override string ToString()
        {
            return this.ID;
        }
        public override int GetHashCode()
        {
            if (String.IsNullOrWhiteSpace(this.ID) == false)
                return this.ID.GetHashCode();

            if (_newRowHashCode == 0)
                _newRowHashCode = System.Threading.Interlocked.Increment(ref GlobalNewRowHasCode);

            return _newRowHashCode;
        }
        public override bool Equals(object obj)
        {
            return obj != null &&
                   obj.GetType().Equals(this.GetType()) &&
                   obj.GetHashCode().Equals(this.GetHashCode());
        }

        #endregion

        #region Utility Properties

        [XmlIgnore]
        public Boolean IsNew
        {
            get { return String.IsNullOrWhiteSpace(_id); }
        }

        #endregion
    }

    /// <summary>
    /// A base class for entities implementing auditing.
    /// </summary>
    [Serializable]
    public abstract class BaseAuditableEntity : BaseEntity, IAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "created_at")]
        public virtual DateTime CreatedAt
        {
            get { return _createdAt; }
            set { _createdAt = value; }
        }
        private DateTime _createdAt = DateTime.Now; // set during new(), not deserialization

        [XmlElement(ElementName = "updated_at")]
        public virtual DateTime UpdatedAt
        {
            get { return _updatedAt; }
            set { _updatedAt = value; }
        }
        private DateTime _updatedAt = DateTime.Now; // set during new(), not deserialization

        [XmlElement(ElementName = "deleted", IsNullable = true)]
        public virtual Boolean? Deleted { get; set; } // na

        #endregion

        #region Utility Properties

        [XmlIgnore]
        public Boolean IsDeleted
        {
            get { return this.Deleted.HasValue && this.Deleted.Value; }
        }

        #endregion
    }
}
