﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Link.Entity.Base;
using Link.Serialization;

namespace Link.Entity.Model
{
    [Serializable]
    [XmlRoot(ElementName = "transformation_route", Namespace = "")]
    public class Route : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "in_component")]
        public Endpoint InComponent { get; set; }

        // Web Service Roue only
        [XmlElement(ElementName = "transformer_component")]
        public TransformerComponent TransformerComponent { get; set; }

        [XmlArray(ElementName = "out_components")]
        [XmlArrayItem(ElementName = "out_component")]
        public List<Endpoint> OutComponents { get; set; }

        // Web Service Roue only
        [XmlElement(ElementName = "response_transformer")]
        public TransformerComponent ResponseComponent { get; set; }

        [XmlElement(ElementName = "status")]
        public Enums.Status Status { get; set; }

        [XmlElement(ElementName = "routeType")]
        public Enums.RouteType RouteType { get; set; }

        [XmlArray(ElementName = "transformer_business_rules")]
        [XmlArrayItem(ElementName = "transformer_business_rule")]
        public List<TransformerBusinessRule> TransformerBusinessRules { get; set; }

        [XmlElement(ElementName = "category")]
        public String Category { get; set; }

        [XmlElement(ElementName = "description")]
        public String Description { get; set; }

        [XmlElement(ElementName = "can_trigger")]
        public Boolean CanTrigger { get; set; }

        [XmlElement(ElementName = "has_schedule")]
        public Boolean HasSchedule { get; set; }

        [XmlElement(ElementName = "route_structure")]
        public RouteStructure Structure { get; set; }

        #endregion
    }

    [XmlRoot(ElementName = "transformation_routes", Namespace = "")]
    public class RouteList : XmlSerializableList<Route> { }
}
