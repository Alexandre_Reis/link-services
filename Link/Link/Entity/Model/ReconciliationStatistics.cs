﻿using System;
using System.Xml.Serialization;
using  Link.Entity.Base;

namespace Link.Entity.Model
{
    [Serializable]
    [XmlRoot(ElementName = "reconciliation_statistics", Namespace = "")]
    public class ReconciliationStatistics : BaseEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "total_rows")]
        public int TotalRows { get; set; }

        [XmlElement(ElementName = "total_left_rows")]
        public int TotalLeftRows { get; set; }

        [XmlElement(ElementName = "total_right_rows")]
        public int TotalRightRows { get; set; }

        [XmlElement(ElementName = "matched_rows_with_variance")]
        public int MatchedRowsWithVariance { get; set; }

        [XmlElement(ElementName = "unmatched_rows")]
        public int UnmatchedRows { get; set; }

        [XmlElement(ElementName = "left_not_in_right")]
        public int LeftNotInRight { get; set; }

        [XmlElement(ElementName = "right_not_in_left")]
        public int RightNotInLeft { get; set; }

        [XmlElement(ElementName = "left_duplicate_rows")]
        public int LeftDuplicateRows { get; set; }

        [XmlElement(ElementName = "right_duplicate_rows")]
        public int RightDuplicateRows { get; set; }

        [XmlElement(ElementName = "matched_percentage")]
        public double MatchedPercentage { get; set; }

        [XmlElement(ElementName = "matched_with_variance_percentage")]
        public double MatchedWithVariancePercentage { get; set; }

        [XmlElement(ElementName = "unmatched_percentage")]
        public double UnmatchedPercentage { get; set; }

        [XmlElement(ElementName = "left_not_in_right_percentage")]
        public double LeftNotInRightPercentage { get; set; }

        [XmlElement(ElementName = "right_not_in_left_percentage")]
        public double RightNotInLeftPercentage { get; set; }

        [XmlElement(ElementName = "left_duplicates_percentage")]
        public double LeftDuplicatesPercentage { get; set; }

        [XmlElement(ElementName = "right_duplicates_percentage")]
        public double RightDuplicatesPercentage { get; set; }

        #endregion
    }

    [XmlRoot(ElementName = "reconciliation_statisticses", Namespace = "")]
    public class ReconciliationStatisticsList : Serialization.XmlSerializableList<ReconciliationStatistics> { }
}
