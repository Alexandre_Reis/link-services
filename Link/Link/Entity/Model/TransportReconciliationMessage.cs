﻿using System;
using System.Xml.Serialization;
using Link.Entity.Base;
using Link.Model;
using Link.Serialization;

namespace Link.Entity.Model
{
    [Serializable]
    [XmlRoot(ElementName = "reconciliation_message", Namespace = "")]
    public class TransportReconciliationMessage : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "status")]
        public Enums.MessageStatus MessageStatus { get; set; }

        [XmlElement(ElementName = "route")]
        public ReconciliationRoute ReconciliationRoute { get; set; }

        [XmlElement(ElementName = "statistics")]
        public ReconciliationStatistics ReconciliationStatistics;

        [XmlElement(ElementName = "last_duration")]
        public int LastDuration { get; set; }

        #endregion

    }

    [XmlRoot(ElementName = "reconciliation_messages", Namespace = "")]
    public class TransportReconciliationMessageList : XmlSerializableList<TransportReconciliationMessage> { }
}
