﻿using System;
using System.Xml.Serialization;

namespace Link.Entity.Model
{
    [Serializable]
    public class ReconDataRow
    {
        [XmlAttribute(AttributeName = "key")]
        public String Key { get; set; }

        [XmlAttribute(AttributeName = "source_row")]
        public Int32 SourceRowIndex { get; set; }
    }
}
