﻿using System;
using System.Xml.Serialization;
using Link.Entity.Base;
using Link.Serialization;

namespace Link.Entity.Model
{
    [Serializable]
    [XmlRoot(ElementName = "endpoint_message", Namespace = "")]
    public class EndpointMessage : BaseEntity
    {
        #region Serializable Properties

        //[XmlElement(ElementName = "message")]
        //public Message Message { get; set; }

        [XmlElement(ElementName = "endpoint")]
        public Endpoint Endpoint { get; set; }

        [XmlElement(ElementName = "status")]
        public Enums.EndpointMessageStatus Status { get; set; }

        #endregion
    }

    [XmlRoot(ElementName = "endpoint_messages", Namespace = "")]
    public class EndpointMessageList : XmlSerializableList<EndpointMessage> { }
}
