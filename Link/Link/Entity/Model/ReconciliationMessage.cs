﻿using System;

namespace Link.Entity.Model
{
    public class ReconciliationMessage 
    {
        public Enums.MessageStatus MessageStatus { get; set; }

        public string RouteId { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
        
        public int LastDuration { get; set; }

        public string MessageId { get; set; }
    }
}
