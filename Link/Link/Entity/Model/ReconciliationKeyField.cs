﻿using System;
using System.Xml.Serialization;
using Link.Entity.Base;

namespace Link.Entity.Model
{
    [Serializable]
    //[XmlRoot(ElementName = "reconciliation_key_field", Namespace = "")]
    public class ReconciliationKeyField : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "left_field")]
        public TransformationField LeftField { get; set; }

        [XmlElement(ElementName = "right_field")]
        public TransformationField RightField { get; set; }

        #endregion
    }
}
