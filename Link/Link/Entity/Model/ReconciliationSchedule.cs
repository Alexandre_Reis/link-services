﻿using System;
using System.Xml.Serialization;
using Link.Entity.Base;

namespace Link.Entity.Model
{
    [Serializable]
    [XmlRoot(ElementName = "reconciliation_schedule", Namespace = "")]
    public class ReconciliationSchedule : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "cron_string")]
        public String CronString { get; set; }

        [XmlElement(ElementName = "manual_trigger")]
        public Boolean IsManualTrigger { get; set; }

        #endregion
    }
}
