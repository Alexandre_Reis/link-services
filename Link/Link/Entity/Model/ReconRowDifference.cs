﻿using System;
using System.Xml.Serialization;

namespace Link.Entity.Model
{
    public class ReconRowDifference
    {
        [XmlElement(ElementName = "leftField")]
        public TransformationField LeftField { get; set; }

        [XmlAttribute(AttributeName = "leftValue")]
        public String LeftValue { get; set; }

        [XmlElement(ElementName = "rightField")]
        public TransformationField RightField { get; set; }

        [XmlAttribute(AttributeName = "rightValue")]
        public String RightValue { get; set; }

        [XmlAttribute(AttributeName = "key")]
        public String Key { get; set; }

        [XmlAttribute(AttributeName = "left_source_row")]
        public Int32 LeftSourceRowIndex { get; set; }

        [XmlAttribute(AttributeName = "right_source_row")]
        public Int32 RightSourceRowIndex { get; set; }
    }
}
