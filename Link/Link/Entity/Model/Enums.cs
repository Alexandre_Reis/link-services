﻿
namespace Link.Entity.Model
{
    public class Enums
    {
        public enum DocumentStatus
        {
            PENDING,
            SUCCESS,
            ERROR
        }

        public enum UserStatus
        {
            ACTIVE,
            INACTIVE,
            PASSWORD_EXPIRED
        }

        public enum EndpointType
        {
            FILE,
            JMS_ACTIVEMQ,
            JMS_WEBSPHERE,
            EMAIL,
            FTP,
            SFTP,
            SSH,
            JDBC,
            SCHEDULE,
            FIX,
            PARENT,
            WEBSERVICE,
            MSMQ
        }

        public enum QueueType
        {
            PUBLISH,
            SUBSCRIBE
        }

        public enum Status
        {
            ENABLED,
            DISABLED
        }

        public enum MessageStatus
        {
            PENDING,
            SUCCESS,
            ERROR,
            WARNING
        }

        public enum SettlementStatus
        {
            PENDING,
            SENDING,
            ERROR,
            NO_FUNDS,
            SENT,
            CANCELED
        }

        public enum EndpointMessageStatus
        {
            SEND_PENDING,
            SEND_SUCCESS,
            SEND_ERROR
        }

        public enum TransformationStatus
        {
            NO_TRANSFORM,
            TRANSFORM_PENDING,
            TRANSFORM_SUCCESS,
            TRANSFORM_ERROR
        }

        public enum FieldType
        {
            INTEGER,
            DECIMAL,
            STRING
        }

        public enum VarianceType
        {
            NONE,
            INTEGER,
            DECIMAL,
            PERCENT
        }

        public enum EndpointAttributeType
        {
            STRING,
            INTEGER,
            DOUBLE,
            BOOLEAN,
            PASSWORD,
            FILE,
            FILELOCATION,
            TIME,
            DAYS
        }

        public enum RouteType
        {
            ATLAS_LINK,
            WEBSERVICE
        }

        public enum RouteStatus
        {
            PAUSED,
            RUNNING
        }

        public enum ComponentType
        {
            TRANSFORMATION,
            RECONCILIATION,
        }

        public enum ComponentDirection
        {
            INBOUND,
            OUTBOUND,
        }

        public enum ReconciliationCategory
        {
            UNMATCHED,
            MISSING_IN_A,
            MISSING_IN_B,
            DUPLICATE_IN_A,
            DUPLICATE_IN_B,
            MATCHED_BY_VARIANCE,
            MATCHED
        }

        public enum PersonType
        {
            FISICA,
            JURIDICA
        }

        public enum AccountType
        {
            INTERNAL,
            EXTERNAL
        }

        public enum TradeType
        {
            C, V, B, D, R
        }

        public enum StockExchange
        {
            BOVESPA, CBLC
        }

        public enum Market
        {
            RV,
            EA,
            RF,
            FU,
            SW,
            FI,
            CB,
            MT,
            MG,
            TV,
            TF,
            OP
        }

        public enum TransactionType
        {
            BSBK,
            COLI,
            COLO,
            OWNI,
            REPU,
            RVPO,
            SECB,
            SECL,
            TRAD,
            SETT,
            EXER,
            FRWD,
            OPTI,
            TEXE
        }

        public enum TradeStatus
        {
            MA,
            SA,
            UN
        }


        public enum ReleaseStatus
        {
            S,
            N,
            I
        }

        /// <summary>
        /// 
        /// </summary>
        public enum BlockStatus
        {
            BLOCKED,
            UNBLOCKED
        }

        /// <summary>
        /// 
        /// </summary>
        public enum DbEntityModel
        {
            ECD,
            BS
        }
    }
}
