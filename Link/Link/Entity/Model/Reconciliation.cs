﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Link.Entity.Base;

namespace Link.Entity.Model
{
    [Serializable]
    [XmlRoot(ElementName = "reconciliation", Namespace = "")]
    public class Reconciliation : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "name")]
        public String Name { get; set; }

        [XmlElement(ElementName = "left_file")]
        public Transformation LeftFile { get; set; }

        [XmlElement(ElementName = "right_file")]
        public Transformation RightFile { get; set; }

        [XmlArray(ElementName = "key_fields")]
        [XmlArrayItem(ElementName = "reconciliation_key_field")]
        public List<ReconciliationKeyField> KeyFields { get; set; }

        [XmlArray(ElementName = "comp_fields")]
        [XmlArrayItem(ElementName = "reconciliation_comp_field")]
        public List<ReconciliationCompField> CompFields { get; set; }

        #endregion
    }

}
