﻿using System;
using System.Xml.Serialization;
using Link.Entity.Base;

namespace Link.Entity.Model
{
    [Serializable]
    [XmlRoot(ElementName = "field", Namespace = "")]
    public class TransformerComponentField : BaseEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "index")]
        public int Index { get; set; }

        [XmlElement(ElementName = "name")]
        public String Name { get; set; }

        #endregion
    }
}
