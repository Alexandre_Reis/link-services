﻿using System;
using System.Xml.Serialization;
using Link.Entity.Base;


namespace Link.Entity.Model
{
    [Serializable]
    [XmlRoot(ElementName = "reconciliation_comp_field", Namespace = "")]
    public class ReconciliationCompField : BaseEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "left_field")]
        public TransformationField LeftField { get; set; }

        [XmlElement(ElementName = "right_field")]
        public TransformationField RightField { get; set; }

        [XmlElement(ElementName = "field_type")]
        public Enums.FieldType FieldType { get; set; }

        [XmlElement(ElementName = "variance")]
        public Decimal Variance { get; set; }

        [XmlElement(ElementName = "variance_type")]
        public Enums.VarianceType VarianceType { get; set; }

        #endregion
    }
}
