﻿using System;

namespace Link.Core.Client
{
    public static class Utils
    {
        public readonly static String ServerDateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff";

        public static String ToServerDatetime(DateTime dt)
        {
            return dt.ToString(ServerDateTimeFormat);
        }

        public static String ToServerDatetimeRFC1123(DateTime dt)
        {
            DateTimeOffset dateOffset = new DateTimeOffset(dt, TimeZoneInfo.Local.GetUtcOffset(dt));
            return dt.ToUniversalTime().ToString("r");
        }
    }
}
