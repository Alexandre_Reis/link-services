﻿using System;
using Link.Entity.Base;
using Link.Model;

namespace Link.Core.Client
{
    public interface IUpdateClient<TEntity> where TEntity : BaseAuditableEntity
    {
        TEntity Save(TEntity entity);

        StandardResponse Delete(String entityID);
    }
}
