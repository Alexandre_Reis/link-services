﻿using System;
using System.Collections.Generic;
using Link.Entity.Base;

namespace Link.Core.Client
{
    public interface IPollClient<TEntity> where TEntity : BaseAuditableEntity
    {
        IList<TEntity> FetchAll();
        IList<TEntity> FetchUpdates(DateTime lastUpdatedDate);
    }
}