﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Link.Model
{
    /// <summary>
    /// Defines meta-data for the dynamic data entity
    /// </summary>
    public class DynamicFieldDescriptor : PropertyDescriptor
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicFieldDescriptor" /> class.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="displayName">The display name.</param>
        /// <param name="fieldType">Type of the field.</param>
        /// <param name="editable">The editable.</param>
        /// <exception cref="System.ArgumentException">Invalid fieldName argument. Cannot be null or empty</exception>
        public DynamicFieldDescriptor(String fieldName, String displayName, DynamicFieldType fieldType, Boolean editable)
            : base(fieldName, null)
        {
            if (String.IsNullOrWhiteSpace(fieldName))
                throw new ArgumentException("Invalid fieldName argument. Cannot be null or empty");

            _displayName = String.IsNullOrWhiteSpace(displayName) ? fieldName : displayName;
            _description = displayName;

            MaxLength = 0;
            FieldType = fieldType;
            Editable = editable;
            LookupValues = new List<Object>();
            TemplateType = DynamicFieldTemplateType.Text;
            TemplateMask = "";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicFieldDescriptor" /> class.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="displayName">The display name.</param>
        public DynamicFieldDescriptor(String fieldName, String displayName) : this(fieldName, displayName, DynamicFieldType.String, false) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicFieldDescriptor" /> class.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        public DynamicFieldDescriptor(String fieldName) : this(fieldName, "", DynamicFieldType.String, false) { }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the maximum field length.
        /// </summary>
        /// <value>
        /// The maximum length of the field.
        /// </value>
        public Int32 MaxLength { get; set; }

        /// <summary>
        /// Gets the type of the field.
        /// </summary>
        /// <value>
        /// The type of the field.
        /// </value>
        public DynamicFieldType FieldType { get; private set; }

        /// <summary>
        /// Gets the editable frlag value.
        /// </summary>
        /// <value>
        /// The editable value.
        /// </value>
        public Boolean Editable { get; private set; }

        /// <summary>
        /// Gets a list of permissible values.
        /// </summary>
        /// <value>
        /// The lookup values.
        /// </value>
        public IList<Object> LookupValues { get; private set; }

        /// <summary>
        /// Gets or sets the type of the template.
        /// </summary>
        /// <value>
        /// The type of the template.
        /// </value>
        public DynamicFieldTemplateType TemplateType { get; set; }

        /// <summary>
        /// Gets or sets the mask for the template.
        /// </summary>
        /// <value>
        /// The mask of the template.
        /// </value>
        public String TemplateMask { get; set; }

        #endregion

        #region PropertyDescriptor Members

        /*
        public virtual AttributeCollection Attributes { get; }
        public virtual string Category { get; }
        public virtual bool DesignTimeOnly { get; }
        public virtual bool IsBrowsable { get; }

        public virtual TypeConverter Converter { get; }
        public virtual bool IsLocalizable { get; }
        public virtual bool SupportsChangeEvents { get; }         
        */

        /// <summary>
        /// Gets the name that can be displayed in a window, such as a Properties window.
        /// </summary>
        /// <returns>The name to display for the member.</returns>
        public override String DisplayName
        {
            get { return _displayName; }
        }
        private String _displayName = "";

        /// <summary>
        /// Gets the description of the member, as specified in the <see cref="T:System.ComponentModel.DescriptionAttribute" />.
        /// </summary>
        /// <returns>The description of the member. If there is no <see cref="T:System.ComponentModel.DescriptionAttribute" />, the property value is set to the default, which is an empty string ("").</returns>
        public override String Description
        {
            get { return _description; }
        }
        private String _description = "";

        /// <summary>
        /// When overridden in a derived class, gets the type of the component this property is bound to.
        /// </summary>
        /// <returns>A <see cref="T:System.Type" /> that represents the type of component this property is bound to. When the <see cref="M:System.ComponentModel.PropertyDescriptor.GetValue(System.Object)" /> or <see cref="M:System.ComponentModel.PropertyDescriptor.SetValue(System.Object,System.Object)" /> methods are invoked, the object specified might be an instance of this type.</returns>
        public override Type ComponentType
        {
            get { return typeof(DynamicEntityRow); }
        }

        /// <summary>
        /// When overridden in a derived class, gets the type of the property.
        /// </summary>
        /// <returns>A <see cref="T:System.Type" /> that represents the type of the property.</returns>
        public override Type PropertyType
        {
            get
            {
                switch (FieldType) //TODO: revise this to determine if needs extending or even is necessary
                {
                    case DynamicFieldType.Number:
                        return typeof(Decimal);
                    case DynamicFieldType.Boolean:
                        return typeof(Boolean);
                    case DynamicFieldType.DateTime:
                        return typeof(DateTime);
                    case DynamicFieldType.Enum:
                        return typeof(Enum);
                    case DynamicFieldType.Object:
                        return typeof(Object);
                    default:
                        return typeof(String);
                }
            }
        }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether this property is read-only.
        /// </summary>
        /// <returns>true if the property is read-only; otherwise, false.</returns>
        public override bool IsReadOnly
        {
            get { return !Editable; }
        }

        /// <summary>
        /// When overridden in a derived class, gets the current value of the property on a component.
        /// </summary>
        /// <param name="component">The component with the property for which to retrieve the value.</param>
        /// <returns>
        /// The value of a property for a given component.
        /// </returns>
        public override Object GetValue(Object component)
        {
            IDynamicEntityRow entity = (IDynamicEntityRow)component;
            return entity.GetFieldValue(base.Name);
        }
        /// <summary>
        /// When overridden in a derived class, sets the value of the component to a different value.
        /// </summary>
        /// <param name="component">The component with the property value that is to be set.</param>
        /// <param name="value">The new value.</param>
        /// <exception cref="System.Exception"></exception>
        public override void SetValue(Object component, Object value)
        {
            if (IsReadOnly)
                throw new Exception(String.Format("{0} is read-only", base.Name));

            IDynamicEntityRow entity = (IDynamicEntityRow)component;
            entity.SetFieldValue(base.Name, value);
        }
        /// <summary>
        /// When overridden in a derived class, returns whether resetting an object changes its value.
        /// </summary>
        /// <param name="component">The component to test for reset capability.</param>
        /// <returns>
        /// true if resetting the component changes its value; otherwise, false.
        /// </returns>
        public override bool CanResetValue(object component)
        {
            return Editable; // and not key value
        }
        /// <summary>
        /// When overridden in a derived class, resets the value for this property of the component to the default value.
        /// </summary>
        /// <param name="component">The component with the property value that is to be reset to the default value.</param>
        public override void ResetValue(object component)
        {
            IDynamicEntityRow entity = (IDynamicEntityRow)component;
            entity.ResetFieldValue(base.Name);
        }
        /// <summary>
        /// When overridden in a derived class, determines a value indicating whether the value of this property needs to be persisted.
        /// </summary>
        /// <param name="component">The component with the property to be examined for persistence.</param>
        /// <returns>
        /// true if the property should be persisted; otherwise, false.
        /// </returns>
        public override bool ShouldSerializeValue(object component)
        {
            return false;
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return Name;
        }

        #endregion
    }
}