﻿using Link.Serialization;
using System;
using System.Xml.Serialization;

namespace Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "permission", Namespace = "")]
    public class Permission
    {
        #region Serializable Properties

        [XmlElement(ElementName = "code")]
        public string Code { get; set; }

        #endregion
    }

    [XmlRoot(ElementName = "permissions", Namespace = "")]
    public class PermissionList : XmlSerializableList<Permission> { }
}
