﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Link.Entity.Base;
using Link.Entity.Model;

namespace Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "reconciliation_route", Namespace = "")]
    public class ReconciliationRoute : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "left_in_component")]
        public Entity.Model.Endpoint LeftInEndpoint { get; set; }

        [XmlElement(ElementName = "right_in_component")]
        public Entity.Model.Endpoint RightInEndpoint { get; set; }

        [XmlElement(ElementName = "left_transformer_component")]
        public Entity.Model.TransformerComponent LeftInComponent { get; set; }

        [XmlElement(ElementName = "right_transformer_component")]
        public Entity.Model.TransformerComponent RightInComponent { get; set; }

        [XmlArray(ElementName = "out_components")]
        [XmlArrayItem(ElementName = "out_component")]
        public List<Entity.Model.Endpoint> OutEndpoints { get; set; }

        [XmlElement(ElementName = "reconciliation")]
        public Entity.Model.Reconciliation Reconciliation { get; set; }

        [XmlElement(ElementName = "status")]
        public Enums.Status Status { get; set; }

        [XmlElement(ElementName = "category")]
        public String Category { get; set; }

        [XmlElement(ElementName = "description")]
        public String Description { get; set; }

        [XmlElement(ElementName = "reconciliation_schedule")]
        public ReconciliationSchedule Schedule { get; set; }

        #endregion
    }

    [XmlRoot(ElementName = "reconciliation_routes", Namespace = "")]
    public class ReconciliationRouteList : Link.Serialization.XmlSerializableList<ReconciliationRoute> { }
}
