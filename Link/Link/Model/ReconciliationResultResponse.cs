﻿using System.IO;

namespace Link.Model
{
    public class ReconciliationResultResponse : BaseModel
    {
        public string XlsFileName { get; set; }
        public string XlsFilePath { get; set; }
    }
}
