﻿
namespace Link.Model
{
    public class ReconRouteFileLocation : BaseModel
    {
        public string RouteId { get; set; }
        public string SourceFileAname { get; set; }
        public string SourceFileBname { get; set; }
        public string SourceFileApath { get; set; }
        public string SourceFileBpath { get; set; }
        public string OutputPath { get; set; }
    }
}
