﻿using System.Collections.Generic;
using Link.Entity.Model;

namespace Link.Model
{
    public class ReconciliationMessageResponse : BaseModel
    {
        public List<ReconciliationMessage> Messages { get; set; }
    }
}
