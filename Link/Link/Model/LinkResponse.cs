﻿using System;

namespace Link.Model
{
    public class LinkResponse
    {
        public DateTime ServerDate { get; set; }
        public ResponseType ResponseStatus { get; set; }
        public String Text { get; set; }
    }
}
