﻿using System;
using System.Xml.Serialization;

namespace Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "server_node", Namespace = "")]
    public class ServerNode
    {
        #region Serializable Properties

        [XmlElement(ElementName = "ip_address")]
        public String IpAddress { get; set; }

        #endregion
    }
}
