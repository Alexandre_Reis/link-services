﻿using System;
using System.Collections.Generic;

namespace Link.Model
{
    public class LoginResult : BaseModel
    {
        public LogonStatus Status { get; set; }

        public String Message { get; set; }

    }
}
