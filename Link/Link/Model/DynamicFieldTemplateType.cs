﻿namespace Link.Model
{
    public enum DynamicFieldTemplateType
    {
        Text,
        CheckBox,
        ComboBox,
        DatePicker
    }
}