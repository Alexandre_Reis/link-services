﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Link.Model
{
    public class DynamicEntityMetaData
    {
        #region Constructors

        public DynamicEntityMetaData()
        {
            this.FieldDescriptors = new List<DynamicFieldDescriptor>();
        }

        #endregion

        #region Properties

        public List<DynamicFieldDescriptor> FieldDescriptors { get; private set; }

        #endregion

        #region Property Descriptors

        public PropertyDescriptorCollection GetPropertyDescriptorCollection()
        {
            return new PropertyDescriptorCollection(this.FieldDescriptors.ToArray());
        }

        #endregion
    }
}