﻿
namespace Link.Model
{
    public class ReconciliationFilePost
    {
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string StringStreamFile { get; set; }
    }
}
