﻿using System;
using System.Xml.Serialization;

namespace Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "logon_request", Namespace = "")]
    public class LogonRequest
    {
        #region Serializable Properties

        [XmlElement(ElementName = "user_name")]
        public String UserName { get; set; }

        [XmlElement(ElementName = "password")]
        public String Password { get; set; }

        #endregion
    }
}
