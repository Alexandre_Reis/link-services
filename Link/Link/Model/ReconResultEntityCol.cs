﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Link.Model
{
    public class ReconResultEntityCol : DynamicFieldDescriptor
    {
        #region Constructors

        public ReconResultEntityCol(String fieldName, String displayName, Int32 colNbr, ColumnType colType)
            : base(fieldName, displayName)
        {
            ColNbr = colNbr;
            ColType = colType;
        }

        #endregion

        #region Properties

        public Int32 ColNbr { get; private set; }
        public ColumnType ColType { get; private set; }

        #endregion
    }
}
