﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "heartbeat", Namespace = "")]
    public class HeartbeatMessage
    {
        #region Serializable Properties

        [XmlElement(ElementName = "status")]
        public String Status { get; set; }

        [XmlElement(ElementName = "environment")]
        public String Environment { get; set; }

        [XmlElement(ElementName = "version")]
        public String Version { get; set; }

        [XmlArray(ElementName = "server_nodes")]
        [XmlArrayItem(ElementName = "server_node")]
        public List<ServerNode> ServerNodes { get; set; }

        [XmlElement(ElementName = "routes_status")]
        public String RoutesStatus { get; set; }

        [XmlElement(ElementName = "server_date")]
        public DateTime ServerDate { get; set; }

        #endregion
    }
}
