﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace Link.Model
{
    /// <summary>
    /// List for Dynamic Data
    /// </summary>
    public class DynamicEntityList : IList<DynamicEntityRow>, ICollection<DynamicEntityRow>, IEnumerable<DynamicEntityRow>, IList, ICollection, IEnumerable, ITypedList, INotifyPropertyChanged, INotifyCollectionChanged, ISupportInitialize
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicEntityList" /> class.
        /// </summary>
        /// <param name="fieldDescriptors">The field descriptors.</param>
        public DynamicEntityList(IList<DynamicFieldDescriptor> fieldDescriptors)
        {
            _fieldDescriptors = fieldDescriptors;
            _internalList = new List<DynamicEntityRow>();
            this.IsInitializing = false;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicEntityList" /> class.
        /// </summary>
        /// <param name="fieldDescriptors">The field descriptors.</param>
        /// <param name="data">The data.</param>
        public DynamicEntityList(IList<DynamicFieldDescriptor> fieldDescriptors, IList<DynamicEntityRow> data)
        {
            _fieldDescriptors = fieldDescriptors;
            _internalList = data.ToList(); // put the items in a new list
            this.IsInitializing = false;
        }

        #endregion

        #region Fields

        private IList<DynamicFieldDescriptor> _fieldDescriptors;
        private IList<DynamicEntityRow> _internalList;

        #endregion

        #region Properties

        /// <summary>
        /// Determines whether an item in the list has been modified
        /// </summary>
        /// <value>
        /// The is modified.
        /// </value>
        public Boolean IsModified
        {
            get
            {
                for (int i = 0; i < _internalList.Count; i++)
                {
                    if (_internalList[i].IsModified)
                        return true;
                }
                return false;
            }
        }
        /// <summary>
        /// Determines whether the list is initializing;
        /// </summary>
        /// <value>
        /// The initializing value.
        /// </value>
        public Boolean IsInitializing { get; private set; }

        #endregion

        #region ITypedList Members

        /// <summary>
        /// Returns the <see cref="T:System.ComponentModel.PropertyDescriptorCollection" /> that represents the properties on each item used to bind data.
        /// </summary>
        /// <param name="listAccessors">An array of <see cref="T:System.ComponentModel.PropertyDescriptor" /> objects to find in the collection as bindable. This can be null.</param>
        /// <returns>
        /// The <see cref="T:System.ComponentModel.PropertyDescriptorCollection" /> that represents the properties on each item used to bind data.
        /// </returns>
        /// <exception cref="System.Exception"></exception>
        public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
        {
            if (_fieldDescriptors.Count == 0)
                throw new Exception(String.Format("Invalid call to {0}.GetItemProperties(). MetaData has not been defined", this.GetType().FullName));

            return new PropertyDescriptorCollection(_fieldDescriptors.ToArray());
        }
        /// <summary>
        /// Returns the name of the list.
        /// This method is only used in the design-time framework  
        /// and by the obsolete DataGrid control. 
        /// </summary>
        /// <param name="listAccessors">An array of <see cref="T:System.ComponentModel.PropertyDescriptor" /> objects, for which the list name is returned. This can be null.</param>
        /// <returns>
        /// The name of the list.
        /// </returns>
        public string GetListName(PropertyDescriptor[] listAccessors)
        {
            return typeof(DynamicEntityRow).Name;
        }

        #endregion

        #region IList<DynamicEntity> Members

        //TODO: raise property changes here
        //TODO: raise INotifyCollectionChanged

        public int IndexOf(DynamicEntityRow item)
        {
            return this._internalList.IndexOf(item);
        }
        public void Insert(int index, DynamicEntityRow item)
        {
            this._internalList.Insert(index, item);
        }
        public void RemoveAt(int index)
        {
            this._internalList.RemoveAt(index);
        }
        public DynamicEntityRow this[int index]
        {
            get { return this._internalList[index]; }
            set { this._internalList[index] = value; }
        }
        public void Add(DynamicEntityRow item)
        {
            this._internalList.Add(item);
        }
        public void Clear()
        {
            this._internalList.Clear();
        }
        public bool Contains(DynamicEntityRow item)
        {
            return this._internalList.Contains(item);
        }
        public void CopyTo(DynamicEntityRow[] array, int arrayIndex)
        {
            this._internalList.CopyTo(array, arrayIndex);
        }
        public int Count
        {
            get { return this._internalList.Count; }
        }
        public bool IsReadOnly
        {
            get { return false; }
        }
        public bool Remove(DynamicEntityRow item)
        {
            return this._internalList.Remove(item);
        }
        public IEnumerator<DynamicEntityRow> GetEnumerator()
        {
            return this._internalList.GetEnumerator();
        }
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region IList Members

        //TODO: raise property changes here
        //TODO: raise INotifyCollectionChanged

        int IList.Add(object value)
        {
            return ((IList)this._internalList).Add(value);
        }
        void IList.Clear()
        {
            ((IList)this._internalList).Clear();
        }
        bool IList.Contains(object value)
        {
            return ((IList)this._internalList).Contains(value);
        }
        int IList.IndexOf(object value)
        {
            return ((IList)this._internalList).IndexOf(value);
        }
        void IList.Insert(int index, object value)
        {
            ((IList)this._internalList).Insert(index, value);
        }
        bool IList.IsFixedSize
        {
            get { return ((IList)this._internalList).IsFixedSize; }
        }
        bool IList.IsReadOnly
        {
            get { return ((IList)this._internalList).IsReadOnly; }
        }
        void IList.Remove(object value)
        {
            ((IList)this._internalList).Remove(value);
        }
        void IList.RemoveAt(int index)
        {
            ((IList)this._internalList).RemoveAt(index);
        }
        object IList.this[int index]
        {
            get
            {
                return ((IList)this._internalList)[index];
            }
            set
            {
                ((IList)this._internalList)[index] = value;
            }
        }
        void ICollection.CopyTo(Array array, int index)
        {
            ((IList)this._internalList).CopyTo(array, index);
        }
        int ICollection.Count
        {
            get { return ((IList)this._internalList).Count; }
        }
        bool ICollection.IsSynchronized
        {
            get { return ((IList)this._internalList).IsSynchronized; }
        }
        object ICollection.SyncRoot
        {
            get { return ((IList)this._internalList).SyncRoot; }
        }

        #endregion

        #region INotifyPropertyChanged

        /// <summary>
        /// Occurs when [property changed].
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged<T>(System.Linq.Expressions.Expression<Func<T>> propertyExpresssion)
        {
            System.Diagnostics.Contracts.Contract.Requires(propertyExpresssion != null, "Expression is a mandatory parameter");

            
        }
        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;

            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region INotifyCollectionChanged Members

        /// <summary>
        /// Does the collection changed.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="items">The items.</param>
        private void DoCollectionChanged(NotifyCollectionChangedAction action, IList<DynamicEntityRow> items)
        {
            NotifyCollectionChangedEventHandler handler = this.CollectionChanged;

            if (handler != null)
            {
                NotifyCollectionChangedEventArgs args = new NotifyCollectionChangedEventArgs(action);
                handler(this, args);
            }
        }

        /// <summary>
        /// Occurs when [collection changed].
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        #endregion

        #region ISupportInitialize

        /// <summary>
        /// Signals the object that initialization is starting.
        /// </summary>
        public void BeginInit()
        {
            if (this.IsInitializing == false)
            {
                this.IsInitializing = true;
                RaisePropertyChanged(() => this.IsInitializing);
            }
        }
        /// <summary>
        /// Signals the object that initialization is complete.
        /// </summary>
        public void EndInit()
        {
            if (this.IsInitializing == true)
            {
                this.IsInitializing = false;
                RaisePropertyChanged(() => this.IsInitializing);
            }
        }

        #endregion
    }
}