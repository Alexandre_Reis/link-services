﻿using System;
using System.Xml.Serialization;

namespace Link.Model
{
    [Serializable]
    public enum ResponseType
    {
        OK, ERROR
    }

    [Serializable]
    [XmlRoot(ElementName = "response", Namespace = "")]
    public class StandardResponse
    {
        #region Serializable Properties

        [XmlElement(ElementName = "type")]
        public ResponseType ResponseStatus { get; set; }

        [XmlElement(ElementName = "text")]
        public String Text { get; set; }

        #endregion
    }
}
