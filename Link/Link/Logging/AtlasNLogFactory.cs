﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Common.Logging;
using NLog.Targets;

namespace Link.Logging
{
    /// <summary>
    /// A custom logging factory for AtlasBus.
    /// The actual logging is provided by NLog.
    /// </summary>
    public class AtlasNLogFactory : ILoggerFactoryAdapter, IDisposable
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the nlog factory.
        /// </summary>
        /// <value>
        /// The nlog factory.
        /// </value>
        public static NLog.LogFactory NlogFactory { get; set; }
        /// <summary>
        /// Gets or sets the log configuration.
        /// </summary>
        /// <value>
        /// The log configuration.
        /// </value>
        public static LogConfiguration LogConfiguration { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AtlasNLogFactory"/> class.
        /// </summary>
        public AtlasNLogFactory()
        {
            //Loading configuration from XML file
            LoadConfiguration();

            var debugOutput = new List<String>();

            // register our custom types with NLog
            RegisterCustomLoggingTypes();

            // resolve logging configuration
            var config = CreateConfig();
            debugOutput.Add("Logging configuration loaded from - default configuration");

            // initialize NLog logging
            NlogFactory = new NLog.LogFactory(config);
            _noopLogger = new Common.Logging.Simple.NoOpLogger();

            // finally push the current reference to the Common.Logging manager (Spring.Rest uses this manager)
            LogManager.Adapter = this;
        }

        #endregion

        #region Fields

        private readonly ILog _noopLogger;

        #endregion

        #region Utility Members

        private static NLog.Config.LoggingConfiguration CreateConfig()
        {
            // Step 1. Create configuration object 
            var config = new NLog.Config.LoggingConfiguration();

            // Step 2. Create targets and add them to the configuration 
            var fileTarget = new FileTarget();
            config.AddTarget("file", fileTarget);

            // Step 3. Set target properties 
            fileTarget.FileName = LogConfiguration.FileName;
            //fileTarget.ArchiveFileName = "${logDirectory}/dev.{#}.log";
            fileTarget.ArchiveEvery = LogConfiguration.FileArchivePeriod;
            fileTarget.ArchiveNumbering = LogConfiguration.ArchiveNumberingMode;
            fileTarget.MaxArchiveFiles = LogConfiguration.MaxArchiveFiles;
            fileTarget.KeepFileOpen = LogConfiguration.KeepFileOpen;
            fileTarget.Header = string.Join("|", LogConfiguration.Headers);
            fileTarget.Layout = string.Format("${{{0}}}", string.Join("}|${", LogConfiguration.Layouts));
            fileTarget.ConcurrentWrites = LogConfiguration.ConcurrentWrites;

            // Step 4. Define rules
            switch (LogConfiguration.LogLevel)
            {
                case LogLevel.All:
                case LogLevel.Trace:
                    {
                        var rule01 = new NLog.Config.LoggingRule("*", NLog.LogLevel.Trace, fileTarget);
                        config.LoggingRules.Add(rule01);

                        break;
                    }
                case LogLevel.Debug:
                    {
                        var rule01 = new NLog.Config.LoggingRule("*", NLog.LogLevel.Debug, fileTarget);
                        config.LoggingRules.Add(rule01);
                        break;
                    }
                case LogLevel.Info:
                    {
                        var rule01 = new NLog.Config.LoggingRule("*", NLog.LogLevel.Info, fileTarget);
                        config.LoggingRules.Add(rule01);
                        break;
                    }
                case LogLevel.Warn:
                    {
                        var rule01 = new NLog.Config.LoggingRule("*", NLog.LogLevel.Warn, fileTarget);
                        config.LoggingRules.Add(rule01);
                        break;
                    }
                case LogLevel.Error:
                    {
                        var rule01 = new NLog.Config.LoggingRule("*", NLog.LogLevel.Error, fileTarget);
                        config.LoggingRules.Add(rule01);
                        break;
                    }
                case LogLevel.Fatal:
                    {
                        var rule01 = new NLog.Config.LoggingRule("*", NLog.LogLevel.Fatal, fileTarget);
                        config.LoggingRules.Add(rule01);
                        break;
                    }
                case LogLevel.Off:
                    {
                        var rule01 = new NLog.Config.LoggingRule("*", NLog.LogLevel.Off, fileTarget);
                        config.LoggingRules.Add(rule01);
                        break;
                    }
                default:
                    {
                        var rule01 = new NLog.Config.LoggingRule("*", fileTarget);
                        config.LoggingRules.Add(rule01);
                        break;
                    }
            }

            return config;
        }
        private static void RegisterCustomLoggingTypes()
        {
            // process all public types
            foreach (var publicType in typeof(AtlasNLogFactory).Assembly.GetTypes())
            {
                var attribs = publicType.GetCustomAttributes(false);

                // register custom LayoutRenderers
                foreach (var attrib in attribs.OfType<NLog.LayoutRenderers.LayoutRendererAttribute>())
                {
                    NLog.Config.ConfigurationItemFactory.Default.LayoutRenderers.RegisterDefinition(attrib.Name, publicType);
                }

                // register custom Targets
                //...
            }
        }
        private ILog InternalGetLogger(string typeName)
        {
            var localRef = NlogFactory;

            // because of the cross-cutting nature of logging, ensure that a logger is always available
            return localRef != null ? new AtlasNLogger(localRef.GetLogger(typeName)) : _noopLogger;
        }

        private static void DefaultLogConfigurationInit()
        {
            LogConfiguration = new LogConfiguration
            {
                FileName = Path.Combine(AppSettings.Settings.Instance().LogDir, "dev.log"),
                FileArchivePeriod = FileArchivePeriod.Day,
                ArchiveNumberingMode = ArchiveNumberingMode.Rolling,
                MaxArchiveFiles = 7,
                KeepFileOpen = false,
                Headers = new List<string>
                    {
                        "#date",
                        "hpc",
                        "procid",
                        "threadid",
                        "level",
                        "source",
                        "message",
                        "exception"
                    },
                Layouts = new List<string>
                    {
                        "longdate",
                        "qpc:normalize=False:precision=9:seconds=False",
                        "processid",
                        "threadid",
                        "level:uppercase=true",
                        "logger",
                        "message",
                        "exception:format=tostring"
                    },
                ConcurrentWrites = false,

                LogLevel = LogLevel.Info
            };
        }

        #endregion

        #region XmlConfiguration

        /// <summary>
        /// Saves the configuration in XML file.
        /// </summary>
        public void SaveConfiguration()
        {
            using (var file = new StreamWriter(string.Format("{0}\\{1}", AppSettings.Settings.Instance().LogDir, "LogConfig.xml"), false))
            {
                var xmlSerializer = new XmlSerializer(typeof(LogConfiguration));
                xmlSerializer.Serialize(file, LogConfiguration);

                if (NlogFactory != null)
                {
                    NlogFactory.Configuration = CreateConfig();
                    NlogFactory.ReconfigExistingLoggers();
                }
            }
        }

        /// <summary>
        /// Loads the configuration from XML file.
        /// </summary>
        public void LoadConfiguration()
        {
            try
            {
                using (var file = new StreamReader(string.Format("{0}\\{1}", AppSettings.Settings.Instance().LogDir, "LogConfig.xml")))
                {
                    var xmlSerializer = new XmlSerializer(typeof(LogConfiguration));
                    LogConfiguration = xmlSerializer.Deserialize(file) as LogConfiguration;
                }
                if (LogConfiguration != null && string.IsNullOrEmpty(LogConfiguration.FileName))
                {
                    LogConfiguration.FileName = Path.Combine(AppSettings.Settings.Instance().LogDir, "dev.log");
                    SaveConfiguration();
                }
            }
            catch (Exception)
            {
                DefaultLogConfigurationInit();
                SaveConfiguration();
            }
        }

        #endregion

        #region Atlas.Link.Logging.ILogFactory

        /// <summary>
        /// Get a ILog instance by name.
        /// </summary>
        /// <param name="name">The name of the logger</param>
        /// <returns></returns>
        public ILog GetLogger(string name)
        {
            return InternalGetLogger(name);
        }

        /// <summary>
        /// Get a ILog instance by type.
        /// </summary>
        /// <param name="type">The type to use for the logger</param>
        /// <returns></returns>
        public ILog GetLogger(Type type)
        {
            return InternalGetLogger(GetTypeName(type));
        }

        private String GetTypeName(Type type)
        {
            var typeName = type.FullName;

            if (type.IsGenericType && !type.IsGenericTypeDefinition)
            {
                //Atlas.Link.Client.PollingDataSource`1[[Atlas.Link.Model.Message, AtlasBus.Model, Version=4.0.0.0, Culture=neutral, PublicKeyToken=null]]
                var offset = type.FullName.IndexOf('`');

                if (offset > 0)
                {
                    var output = new System.Text.StringBuilder();
                    output.Append(typeName.Substring(0, offset));
                    output.Append("(");

                    var typeParameters = type.GetGenericArguments();

                    for (var i = 0; i < typeParameters.Length; i++)
                    {
                        if (i > 0)
                            output.Append(", ");
                        output.Append(GetTypeName(typeParameters[i]));
                    }

                    output.Append(")");
                    typeName = output.ToString();
                }
            }

            return typeName;
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (NlogFactory != null)
            {
                var local = NlogFactory;
                NlogFactory = null;

                local.Dispose();
            }
        }

        #endregion
    }
}
