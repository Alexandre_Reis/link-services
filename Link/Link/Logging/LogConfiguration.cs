﻿using System.Collections.Generic;
using Common.Logging;
using NLog.Targets;

namespace Link.Logging
{
    /// <summary>
    /// Model for Logging Configuration.
    /// </summary>
    public class LogConfiguration
    {
        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        public string FileName { get; set; }
        /// <summary>
        /// Gets or sets the file archive period.
        /// </summary>
        /// <value>
        /// The file archive period.
        /// </value>
        public FileArchivePeriod FileArchivePeriod { get; set; }
        /// <summary>
        /// Gets or sets the archive numbering mode.
        /// </summary>
        /// <value>
        /// The archive numbering mode.
        /// </value>
        public ArchiveNumberingMode ArchiveNumberingMode { get; set; }
        /// <summary>
        /// Gets or sets the maximum archive files.
        /// </summary>
        /// <value>
        /// The maximum archive files.
        /// </value>
        public int MaxArchiveFiles { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether keeping file open.
        /// </summary>
        /// <value>
        ///   <c>true</c> if keeping file open otherwise, <c>false</c>.
        /// </value>
        public bool KeepFileOpen { get; set; }

        /// <summary>
        /// Gets or sets the headers.
        /// </summary>
        /// <value>
        /// The headers.
        /// </value>
        public List<string> Headers { get; set; }
        /// <summary>
        /// Gets or sets the layouts.
        /// </summary>
        /// <value>
        /// The layouts.
        /// </value>
        public List<string> Layouts { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether concurrent writes.
        /// </summary>
        /// <value>
        ///   <c>true</c> if concurrent writes otherwise, <c>false</c>.
        /// </value>
        public bool ConcurrentWrites { get; set; }

        /// <summary>
        /// Gets or sets the log level.
        /// </summary>
        /// <value>
        /// The log level.
        /// </value>
        public LogLevel LogLevel { get; set; }

        //TODO: Handle more than one level
        //public List<LogLevel> LogLevels { get; set; }
    }
}
