﻿using System.Diagnostics.Contracts;

namespace Link.Logging
{
    /// <summary>
    /// A logger implemented using the Common Logging NLog logger.
    /// </summary>
    public class AtlasNLogger : Common.Logging.NLog.NLogLogger
    {
        #region Construction

        internal AtlasNLogger(NLog.Logger logger)
            : base(logger)
        {
            Contract.Requires(logger != null);
        }

        #endregion
    }
}
