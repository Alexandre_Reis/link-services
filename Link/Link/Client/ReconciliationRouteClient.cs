﻿using System;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Link.Core.Client;
using Link.Model;
using Link.Net;

namespace Link.Client
{
    [ServiceContract]
    public interface IReconciliationRouteService
    {
        #region REST Methods

        [OperationContract]
        ReconciliationRouteList GetAllRoutes();

        [OperationContract]
        ReconciliationRouteList GetUpdates(DateTime lastUpdatedDate);

        [OperationContract]
        ReconciliationRoute Save(ReconciliationRoute route);

        [OperationContract]
        StandardResponse Delete(String routeId);

        [OperationContract]
        StandardResponse Trigger(String routeId);

        [OperationContract]
        StandardResponse Enable(String routeId);

        [OperationContract]
        StandardResponse Disable(String routeId);

        #endregion
    }

    [ServiceHost("", BaseURL = "/reconroute")]
    [XmlSerializerFormat]
    public class ReconciliationRouteClient : SpringRestClient<IReconciliationRouteService>, IReconciliationRouteService, IPollClient<ReconciliationRoute>, IUpdateClient<ReconciliationRoute>
    {
        #region IRouteService

        [WebInvoke(Method = "GET", UriTemplate = "/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public ReconciliationRouteList GetAllRoutes()
        {
            return Execute<ReconciliationRouteList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        [WebInvoke(Method = "GET", UriTemplate = "/updates?date={lastUpdatedDate}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public ReconciliationRouteList GetUpdates(DateTime lastUpdatedDate)
        {
            return Execute<ReconciliationRouteList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(lastUpdatedDate));
        }

        [WebInvoke(Method = "POST", UriTemplate = "/save")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public ReconciliationRoute Save(ReconciliationRoute route)
        {
            return ExecuteWithBody<ReconciliationRoute>(System.Reflection.MethodInfo.GetCurrentMethod(), route);
        }

        [WebInvoke(Method = "DELETE", UriTemplate = "/{routeId}/delete")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Delete(String routeId)
        {
            Execute(System.Reflection.MethodInfo.GetCurrentMethod(), routeId);
            return null; //TODO...
        }

        [WebInvoke(Method = "POST", UriTemplate = "/{routeId}/trigger")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Trigger(String routeId)
        {
            Execute(System.Reflection.MethodInfo.GetCurrentMethod(), routeId);
            return null;
        }

        [WebInvoke(Method = "POST", UriTemplate = "/{routeId}/enabled")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Enable(String routeId)
        {
            return Execute<StandardResponse>(System.Reflection.MethodInfo.GetCurrentMethod(), routeId);
        }

        [WebInvoke(Method = "POST", UriTemplate = "/{routeId}/disabled")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Disable(String routeId)
        {
            return Execute<StandardResponse>(System.Reflection.MethodInfo.GetCurrentMethod(), routeId);
        }

        #endregion

        #region IPollClient

        System.Collections.Generic.IList<ReconciliationRoute> IPollClient<ReconciliationRoute>.FetchAll()
        {
            return GetAllRoutes();
        }

        System.Collections.Generic.IList<ReconciliationRoute> IPollClient<ReconciliationRoute>.FetchUpdates(DateTime lastUpdatedDate)
        {
            return GetUpdates(lastUpdatedDate);
        }

        #endregion
    }
}
