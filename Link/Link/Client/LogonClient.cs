﻿using System;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Link.Model;
using Link.Net;

namespace Link.Client
{
    [ServiceContract]
    public interface ILogonService
    {
        #region REST Methods

        [OperationContract]
        LogonResult LogonUser(LogonRequest logonRequest);

        [OperationContract]
        StandardResponse Logout();

        #endregion
    }

    [ServiceHostAttribute("", BaseURL = "/logon")]
    [XmlSerializerFormat]
    public class LogonClient : SpringRestClient<ILogonService>, ILogonService
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="LogonClient" /> class.
        /// </summary>
        public LogonClient() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogonClient" /> class.
        /// </summary>
        /// <param name="connectionTimeout">The connection timeout.</param>
        public LogonClient(Int32 connectionTimeout) : base(connectionTimeout) { }

        #endregion

        #region ILogonService

        [WebInvoke(Method = "POST", UriTemplate = "/")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public LogonResult LogonUser(LogonRequest request)
        {
            // set the authorization header for every call
            this.Session.SetCredentials(request.UserName, request.Password);

            LogonResult result = null;

            try
            {
                result = ExecuteWithBody<LogonResult>(System.Reflection.MethodInfo.GetCurrentMethod(), request);

                Logger.InfoFormat("Logon succeeded for user: {0}", request.UserName);
            }
            catch (Spring.Rest.Client.HttpClientErrorException ex)
            {
                Logger.InfoFormat("Logon failed for user: {0}", request.UserName);

                if (ex.Message.Contains("401 - Unauthorized"))
                {
                    result = new LogonResult { Status = LogonStatus.INVALID_CREDENTIALS, Message = "Invalid Credentials" };
                }
                else
                    if (ex.Message.Contains("404 - NotFound"))
                    {
                        result = new LogonResult { Status = LogonStatus.ERROR, Message =  "Logon Endpoint not found" };
                    }
                    else
                    {
                        throw;
                    }
            }
            catch (Exception ex)
            {
                result = new LogonResult { Status = LogonStatus.ERROR, Message = ex.Message };
                Logger.InfoFormat("Logon failed for user: {0}", request.UserName);
            }

            return result;
        }

        [WebInvoke(Method = "POST", UriTemplate = "/logout")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Logout()
        {
            return Execute<StandardResponse>(System.Reflection.MethodInfo.GetCurrentMethod());
        }


        #endregion
    }
}
