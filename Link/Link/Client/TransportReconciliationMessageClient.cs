﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Link.Core.Client;
using Link.Entity.Model;
using Link.Net;

namespace Link.Client
{
    [ServiceContract]
    public interface ITransportReconciliationMessageClient
    {
        #region REST Methods

        [OperationContract]
        TransportReconciliationMessageList GetAllMessages();

        [OperationContract]
        TransportReconciliationMessageList GetMessagesBetween(DateTime startDate, DateTime endDate, Boolean updatesOnly = false);

        [OperationContract]
        TransportReconciliationMessageList GetMessagesFrom(DateTime startDate, Boolean updatesOnly = false);

        [OperationContract]
        TransportReconciliationMessageList GetMessagesUntil(DateTime endDate, Boolean updatesOnly = false);


        [OperationContract]
        TransportReconciliationMessageList GetUpdates(DateTime lastUpdatedDate);

        #endregion
    }

    [XmlSerializerFormat]
    [ServiceHostAttribute("Reconciliation", BaseURL = "/reconmessage")]
    public class TransportReconciliationMessageClient : SpringRestClient<ITransportReconciliationMessageClient>, ITransportReconciliationMessageClient, IPollClient<TransportReconciliationMessage>
    {
        #region IReconMessageService

        public TransportReconciliationMessageList GetMessages(DateTime? startDateTime, DateTime? endDateTime, Boolean updatesOnly, String messageStatus)
        {
            if (startDateTime.HasValue && endDateTime.HasValue)
            {
                if (String.IsNullOrWhiteSpace(messageStatus))
                    return this.GetMessagesBetween(startDateTime.Value, endDateTime.Value, updatesOnly);

                return this.GetMessagesBetween(startDateTime.Value, endDateTime.Value, updatesOnly, messageStatus);
            }
            if (startDateTime.HasValue)
            {
                if (String.IsNullOrWhiteSpace(messageStatus))
                    return this.GetMessagesFrom(startDateTime.Value, updatesOnly);

                return this.GetMessagesFrom(startDateTime.Value, updatesOnly, messageStatus);
            }
            if (endDateTime.HasValue)
            {
                if (String.IsNullOrWhiteSpace(messageStatus))
                    return this.GetMessagesUntil(endDateTime.Value, updatesOnly);

                return this.GetMessagesUntil(endDateTime.Value, updatesOnly, messageStatus);
            }

            return this.GetAllMessages();
        }

        [WebInvoke(Method = "GET", UriTemplate = "/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public TransportReconciliationMessageList GetAllMessages()
        {
            return Execute<TransportReconciliationMessageList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        [WebInvoke(Method = "GET", UriTemplate = "/updates?date={lastUpdatedDate}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public TransportReconciliationMessageList GetUpdates(DateTime lastUpdatedDate)
        {
            return Execute<TransportReconciliationMessageList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(lastUpdatedDate));
        }

        //    @RequestMapping(value = "getDetails/messageId", method = RequestMethod.GET)
        //    public @ResponseBody ReconciliationResult getDetail(@RequestParam("messageId") String messageId) {

        [WebInvoke(Method = "GET", UriTemplate = "/{messageId}/getDetail")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public ReconciliationResult GetDetail(String messageId)
        {
            return Execute<ReconciliationResult>(System.Reflection.MethodInfo.GetCurrentMethod(), messageId);
        }


        [WebInvoke(Method = "GET", UriTemplate = "/list?startDate={startDate}&endDate={endDate}&updatesOnly={updatesOnly}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public TransportReconciliationMessageList GetMessagesBetween(DateTime startDate, DateTime endDate, Boolean updatesOnly = false)
        {
            return Execute<TransportReconciliationMessageList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(startDate), Utils.ToServerDatetime(endDate), updatesOnly);
        }

        [WebInvoke(Method = "GET", UriTemplate = "/list?startDate={startDate}&endDate={endDate}&updatesOnly={updatesOnly}&messageStatus={messageStatus}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public TransportReconciliationMessageList GetMessagesBetween(DateTime startDate, DateTime endDate, Boolean updatesOnly, String messageStatus)
        {
            return Execute<TransportReconciliationMessageList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(startDate), Utils.ToServerDatetime(endDate), updatesOnly, messageStatus);
        }

        [WebInvoke(Method = "GET", UriTemplate = "/list?startDate={startDate}&updatesOnly={updatesOnly}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public TransportReconciliationMessageList GetMessagesFrom(DateTime startDate, Boolean updatesOnly = false)
        {
            return Execute<TransportReconciliationMessageList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(startDate), updatesOnly);
        }

        [WebInvoke(Method = "GET", UriTemplate = "/list?startDate={startDate}&updatesOnly={updatesOnly}&messageStatus={messageStatus}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public TransportReconciliationMessageList GetMessagesFrom(DateTime startDate, Boolean updatesOnly, String messageStatus)
        {
            return Execute<TransportReconciliationMessageList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(startDate), updatesOnly, messageStatus);
        }

        [WebInvoke(Method = "GET", UriTemplate = "/list?endDate={endDate}&updatesOnly={updatesOnly}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public TransportReconciliationMessageList GetMessagesUntil(DateTime endDate, Boolean updatesOnly = false)
        {
            return Execute<TransportReconciliationMessageList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(endDate), updatesOnly);
        }

        [WebInvoke(Method = "GET", UriTemplate = "/list?endDate={endDate}&updatesOnly={updatesOnly}&messageStatus={messageStatus}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public TransportReconciliationMessageList GetMessagesUntil(DateTime endDate, Boolean updatesOnly, String messageStatus)
        {
            return Execute<TransportReconciliationMessageList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(endDate), updatesOnly, messageStatus);
        }

        #endregion

        #region IPollClient<ReconciliationMessage>

        public IList<TransportReconciliationMessage> FetchAll()
        {
            return GetAllMessages();
        }
        public IList<TransportReconciliationMessage> FetchUpdates(DateTime lastUpdatedDate)
        {
            return GetUpdates(lastUpdatedDate);
        }

        #endregion

    }
}
