﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Link.ViewModel
{
    /// <summary>
    /// Use this attribute on ViewModel properties to control the event beahviour when RaiseProperty is called.
    /// </summary>
    [AttributeUsageAttribute(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class SuppressPropertyChangeBehaviourAttribute : Attribute
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="SuppressPropertyChangeBehaviourAttribute"/> class.
        /// </summary>
        public SuppressPropertyChangeBehaviourAttribute()
        {
            this.BypassCommandStateCheck = false;
            this.BypassValidationCheck = false;
            this.BypassModifiedFlag = false;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether to bypass the validation check.
        /// </summary>
        /// <value>
        /// <c>true</c> if you want to bypass validation checks; otherwise, <c>false</c>.
        /// </value>
        public Boolean BypassValidationCheck { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to bypass the command state check.
        /// </summary>
        /// <value>
        /// <c>true</c> if you want to bypass the command state check; otherwise, <c>false</c>.
        /// </value>
        public Boolean BypassCommandStateCheck { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to bypass setting the modified flag.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [bypass set modified]; otherwise, <c>false</c>.
        /// </value>
        public Boolean BypassModifiedFlag { get; set; }

        #endregion
    }
}
