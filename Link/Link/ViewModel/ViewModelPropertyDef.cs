﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Link.Core;

namespace Link.ViewModel
{
    internal class ViewModelPropertyDef
    {
        #region Constructors

        public ViewModelPropertyDef(System.Reflection.PropertyInfo propInfo)
        {
            this.IsReadonly = false;
            this.BypassCommandStateCheck = false;
            this.BypassValidationCheck = false;
            this.BypassModifiedFlag = false;

            // get the suppression attribute (if specified)
            SuppressPropertyChangeBehaviourAttribute suppressAttrib = (Attribute.GetCustomAttribute(propInfo, typeof(SuppressPropertyChangeBehaviourAttribute)) as SuppressPropertyChangeBehaviourAttribute);

            if (suppressAttrib != null)
            {
                this.BypassCommandStateCheck = suppressAttrib.BypassCommandStateCheck;
                this.BypassValidationCheck = suppressAttrib.BypassValidationCheck;
                this.BypassModifiedFlag = suppressAttrib.BypassModifiedFlag;
            }

            // get a reference to the getter method
            this.GetterMethodHandle = propInfo.GetGetMethod().MethodHandle;

            // get a validation method delegate (if defined)
            MethodInfo info = propInfo.DeclaringType.FindStaticMethods()                  // static method
                                                    .WithName("Validate" + propInfo.Name) // by-convention validation
                                                    .WithInputParams(typeof(Object), typeof(PropertyValidationResult))
                                                    .FirstOrDefault();

            if (info != null)
                this.ValidationMethod = (PropertyValidationDelegate)PropertyValidationDelegate.CreateDelegate(typeof(PropertyValidationDelegate), info);
        }

        #endregion

        #region Properties

        public Boolean IsReadonly { get; private set; }
        public Boolean BypassCommandStateCheck { get; private set; }
        public Boolean BypassValidationCheck { get; private set; }
        public Boolean BypassModifiedFlag { get; private set; }
        public RuntimeMethodHandle GetterMethodHandle { get; private set; }
        public PropertyValidationDelegate ValidationMethod { get; private set; }

        #endregion
    }
}
