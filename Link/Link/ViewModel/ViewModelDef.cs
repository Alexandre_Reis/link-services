﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Common.Logging;
using Link.Core;

namespace Link.ViewModel
{
    internal class ViewModelDef
    {
        private static Type ValidatorResultType = typeof(PropertyValidationResult);

        #region Construction

        public ViewModelDef(Type viewmodelType)
        {
            this.Logger = Common.Logging.LogManager.GetLogger(viewmodelType);
            this.DelayedNewValidation = true; //TODO: implement from attribute
            this.Properties = new Dictionary<String, ViewModelPropertyDef>();
            this.ValidationMethod = null;

            MethodInfo info = viewmodelType.FindStaticMethods()
                                           .WithName("Validate" + viewmodelType.Name)
                                           .WithInputParams(typeof(Object), typeof(EntityValidationResult))
                                           .FirstOrDefault();

            if (info != null)
                this.ValidationMethod = (EntityValidationDelegate)EntityValidationDelegate.CreateDelegate(typeof(EntityValidationDelegate), info);

            foreach (var item in viewmodelType.GetProperties(BindingFlags.Instance | BindingFlags.Public).Where(x => x.CanRead && x.CanWrite))
            {
                this.Properties.Add(item.Name, new ViewModelPropertyDef(item));
            }
        }

        #endregion

        #region Properties

        public ILog Logger { get; private set; }
        public Boolean DelayedNewValidation { get; private set; }
        public Dictionary<String, ViewModelPropertyDef> Properties { get; private set; }
        public EntityValidationDelegate ValidationMethod { get; private set; }

        #endregion
    }
}
