﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Link.Entity.Base;
using Link.ViewModel;

namespace Atlas.Link.ViewModels
{
    public class EntityViewModel<TEntity> : BaseViewModel where TEntity : BaseEntity
    {
        #region Construction

        public EntityViewModel(TEntity modelEntity)
        {
            this.ModelEntity = modelEntity;

            //TODO: clone entity here for previous value in order to implement HasChanged
        }

        #endregion

        #region Utility Members

        protected TEntity ModelEntity { get; private set; }

        public TEntity GetModelEntity()
        {
            return this.ModelEntity;
        }
        protected void SetModelEntity(TEntity entity)
        {
            this.ModelEntity = entity;
        }

        #endregion

        #region Bindable Properties

        public String ID
        {
            get { return this.ModelEntity.ID; }
        }

        public virtual Boolean IsNew
        {
            get { return this.ModelEntity.IsNew; }
        }
        //public virtual Boolean IsModified
        //{
        //    get { return ...; }
        //}

        #endregion

        #region Binding Command

        private void DoCloseCommand()
        {
            DoRequestViewClose(true);
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
        
            return this.ModelEntity.ToString();
        }
        public override int GetHashCode()
        {
            return this.ModelEntity.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return obj != null &&
                   obj.GetType().Equals(this.GetType()) &&
                   obj.GetHashCode().Equals(this.GetHashCode());
        }

        #endregion
    }
}