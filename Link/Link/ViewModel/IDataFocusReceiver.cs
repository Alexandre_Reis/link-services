﻿using System;


namespace Link.ViewModel
{
    public interface IDataFocusReceiver
    {
        void PositionChanged(Int32 sourceRowNbr, String sourceFieldName);
    }
}
