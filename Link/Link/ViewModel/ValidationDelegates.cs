﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Link.ViewModel
{
    /// <summary>
    /// The signature for static property validation members.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="result">The result.</param>
    public delegate void PropertyValidationDelegate(Object value, PropertyValidationResult result);

    /// <summary>
    /// The signature for static entity validation members.
    /// </summary>
    /// <param name="entity">The entity.</param>
    /// <param name="resultSet">The result set.</param>
    public delegate void EntityValidationDelegate(Object entity, EntityValidationResult resultSet);
}
