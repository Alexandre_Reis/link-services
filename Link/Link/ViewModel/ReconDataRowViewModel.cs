﻿using System;
using Link.Entity.Model;

namespace Link.ViewModel
{
    public class ReconDataRowViewModel
    {
        #region Constructors

        public ReconDataRowViewModel(ReconDataRow entity)
        {
            _entity = entity;
        }

        #endregion

        #region Fields

        private ReconDataRow _entity;

        #endregion

        #region Binding Properties

        public String Key { get; set; }
        public Int32 SourceRowIndex { get; set; }

        #endregion
    }
}