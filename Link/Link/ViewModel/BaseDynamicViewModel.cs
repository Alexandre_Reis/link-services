﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;
using Link.Model;

namespace Link.ViewModel
{
    /// <summary>
    /// Base ViewModel for Dynamic Data
    /// </summary>
    public class BaseDynamicViewModel : IList<DynamicEntityRow>, IList, ITypedList, INotifyPropertyChanged 
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseDynamicViewModel"/> class.
        /// </summary>
        public BaseDynamicViewModel()
        {
            MetaData = new DynamicEntityMetaData();
            DynamicData = new List<DynamicEntityRow>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the meta data for the viewmodel.
        /// </summary>
        /// <value>
        /// The meta data.
        /// </value>
        protected internal DynamicEntityMetaData MetaData { get; private set; }
        /// <summary>
        /// Gets the dynamic data.
        /// </summary>
        /// <value>
        /// The dynamic data.
        /// </value>
        public  List<DynamicEntityRow> DynamicData { get; private set; }

        #endregion

        #region ITypedList Members

        /// <summary>
        /// Get the field descriptor for the given field name.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public DynamicFieldDescriptor GetDynamicFieldDescriptor(String fieldName)
        {
            return MetaData.FieldDescriptors.FirstOrDefault(x => x.Name.Equals(fieldName, StringComparison.InvariantCultureIgnoreCase));
        }

        public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
        {
            if (MetaData.FieldDescriptors.Count == 0)
                throw new Exception(String.Format("Invalid call to {0}.GetItemProperties(). MetaData has not been defined", GetType().FullName));

            return MetaData.GetPropertyDescriptorCollection();
        }
        // This method is only used in the design-time framework  
        // and by the obsolete DataGrid control. 
        public string GetListName(PropertyDescriptor[] listAccessors)
        {
            return typeof(DynamicEntityRow).Name;
        }

        #endregion

        #region INotifyPropertyChanged

        /// <summary>
        /// Occurs when [property changed].
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises the property changed.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyExpresssion">The property expresssion.</param>
        protected void RaisePropertyChanged<T>(Expression<Func<T>> propertyExpresssion)
        {
            Contract.Requires(propertyExpresssion != null, "Expression is a mandatory parameter");

            
        }
        /// <summary>
        /// Raises the property changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region IList<DynamicEntity> Members

        public int IndexOf(DynamicEntityRow item)
        {            
            return DynamicData.IndexOf(item);
        }
        public void Insert(int index, DynamicEntityRow item)
        {
            DynamicData.Insert(index, item);
        }
        public void RemoveAt(int index)
        {
            DynamicData.RemoveAt(index);
        }
        public DynamicEntityRow this[int index]
        {
            get { return DynamicData[index]; }
            set { DynamicData[index] = value; }
        }
        public void Add(DynamicEntityRow item)
        {
            DynamicData.Add(item);
        }
        public void Clear()
        {
            DynamicData.Clear();
        }
        public bool Contains(DynamicEntityRow item)
        {
            return DynamicData.Contains(item);
        }
        public void CopyTo(DynamicEntityRow[] array, int arrayIndex)
        {
            DynamicData.CopyTo(array, arrayIndex);
        }
        public int Count
        {
            get { return DynamicData.Count; }
        }
        public bool IsReadOnly
        {
            get { return false; }
        }
        public bool Remove(DynamicEntityRow item)
        {
            return DynamicData.Remove(item);
        }
        public IEnumerator<DynamicEntityRow> GetEnumerator()
        {
            return DynamicData.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region IList Members

        int IList.Add(object value)
        {
            return ((IList)DynamicData).Add(value);
        }
        void IList.Clear()
        {
            ((IList)DynamicData).Clear();
        }
        bool IList.Contains(object value)
        {
            return ((IList)DynamicData).Contains(value);
        }
        int IList.IndexOf(object value)
        {
            return ((IList)DynamicData).IndexOf(value);
        }
        void IList.Insert(int index, object value)
        {
            ((IList)DynamicData).Insert(index, value);
        }
        bool IList.IsFixedSize
        {
            get { return ((IList)DynamicData).IsFixedSize; }
        }
        bool IList.IsReadOnly
        {
            get { return ((IList)DynamicData).IsReadOnly; }
        }
        void IList.Remove(object value)
        {
            ((IList)DynamicData).Remove(value);
        }
        void IList.RemoveAt(int index)
        {
            ((IList)DynamicData).RemoveAt(index);
        }
        object IList.this[int index]
        {
            get
            {
                return ((IList)DynamicData)[index];
            }
            set
            {
                ((IList)DynamicData)[index] = value;
            }
        }
        void ICollection.CopyTo(Array array, int index)
        {
            ((IList)DynamicData).CopyTo(array, index);
        }
        int ICollection.Count
        {
            get { return ((IList)DynamicData).Count; }
        }
        bool ICollection.IsSynchronized
        {
            get { return ((IList)DynamicData).IsSynchronized; }
        }
        object ICollection.SyncRoot
        {
            get { return ((IList)DynamicData).SyncRoot; }
        }

        #endregion
    }
}