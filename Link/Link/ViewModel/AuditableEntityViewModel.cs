﻿using System;
using Atlas.Link.ViewModels;
using Link.Entity.Base;

namespace Link.ViewModel
{
    public class AuditableEntityViewModel<TEntity> : EntityViewModel<TEntity> where TEntity : BaseAuditableEntity
    {
        #region Construction

        public AuditableEntityViewModel(TEntity modelEntity)
            : base(modelEntity)
        {
            
        }

        #endregion

        #region Bindable Properties

        public DateTime CreatedAt
        {
            get { return this.ModelEntity.CreatedAt; }

        }

        public DateTime UpdatedAt
        {
            get { return this.ModelEntity.UpdatedAt; }
        }

        public Boolean Deleted
        {
            get { return this.ModelEntity.Deleted.HasValue && this.ModelEntity.Deleted.Value; }
        }

        #endregion

        #region Binding Command

        

        protected virtual Boolean BeforeUpdateCommand(TEntity entity)
        {
            return true;
        }
        protected virtual void ExecuteSaveCommand()
        {
            
        }
        protected virtual void HandleSaveCommandError(TEntity entity, Exception ex)
        {
            
        }
        protected virtual void ExecuteCancelCommand()
        {
            DoRequestViewClose(false);
        }
        protected override void DoSetCommandState()
        {
            base.DoSetCommandState();

            
        }

        #endregion
    }
}