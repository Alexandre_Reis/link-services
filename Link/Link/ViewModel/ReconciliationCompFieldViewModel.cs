﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Link.ViewModels;
using Link.Entity.Model;

namespace Link.ViewModel
{
    /// <summary>
    /// ViewModel for the Reconciliation entity.
    /// </summary>
    public class ReconciliationCompFieldViewModel : EntityViewModel<ReconciliationCompField>
    {
        #region Construction

        public ReconciliationCompFieldViewModel(ReconciliationCompField modelEntity)
            : base(modelEntity)
        {
            if (modelEntity.LeftField == null)
                modelEntity.LeftField = new TransformationField { Name = "", Position = 0 };
            if (modelEntity.RightField == null)
                modelEntity.RightField = new TransformationField { Name = "", Position = 0 };

            _leftField = new TransformationFieldViewModel(modelEntity.LeftField);
            _rightField = new TransformationFieldViewModel(modelEntity.RightField);
        }

        #endregion

        #region Bindable Lookup Collections

        public IList<Enums.FieldType> FieldTypeList
        {
            get { return null; }
        }
        public IList<Enums.VarianceType> VarianceTypeList
        {
            get
            {
                return null;
            }
        }

        #endregion

        #region Bindable Entity Properties

        public TransformationFieldViewModel LeftField
        {
            get { return _leftField; }
            set
            {
                _leftField = value;
                ModelEntity.LeftField = (_leftField != null) ? _leftField.GetModelEntity() : null;
                RaisePropertyChanged(() => LeftField);
            }
        }
        private TransformationFieldViewModel _leftField;

        public TransformationFieldViewModel RightField
        {
            get { return _rightField; }
            set
            {
                _rightField = value;
                ModelEntity.RightField = (_rightField != null) ? _rightField.GetModelEntity() : null;
                RaisePropertyChanged(() => RightField);
            }
        }
        private TransformationFieldViewModel _rightField;

        public Enums.FieldType FieldType { get; set; }

        public Decimal Variance
        {
            get { return ModelEntity.Variance; }
            set
            {
                if (ModelEntity.Variance != value)
                {
                    ModelEntity.Variance = value;
                    RaisePropertyChanged(() => Variance);
                }
            }
        }

        public Enums.VarianceType VarianceType { get; set; }

        #endregion

        #region Validation

        private static void ValidateLeftField(Object value, PropertyValidationResult result)
        {
            
        }
        private static void ValidateRightField(Object value, PropertyValidationResult result)
        {
            
        }
        

        #endregion
    }
}