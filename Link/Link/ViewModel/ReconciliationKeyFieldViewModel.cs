﻿using System;
using Atlas.Link.ViewModels;
using Link.Entity.Model;

namespace Link.ViewModel
{
    /// <summary>
    /// ViewModel for the Reconciliation entity.
    /// </summary>
    public class ReconciliationKeyFieldViewModel : EntityViewModel<ReconciliationKeyField>
    {
        #region Construction

        public ReconciliationKeyFieldViewModel(ReconciliationKeyField modelEntity)
            : base(modelEntity)
        {
            if (modelEntity.LeftField == null)
                modelEntity.LeftField = new TransformationField { Name = "", Position = 0 };
            if (modelEntity.RightField == null)
                modelEntity.RightField = new TransformationField { Name = "", Position = 0 };

            _leftField = new TransformationFieldViewModel(modelEntity.LeftField);
            _rightField = new TransformationFieldViewModel(modelEntity.RightField);
        }

        #endregion

        #region Bindable Entity Properties

        public TransformationFieldViewModel LeftField
        {
            get { return _leftField; }
            set
            {
                _leftField = value;
                this.ModelEntity.LeftField = (_leftField != null) ? _leftField.GetModelEntity() : null;
                RaisePropertyChanged(() => this.LeftField);
            }
        }
        private TransformationFieldViewModel _leftField;

        public TransformationFieldViewModel RightField
        {
            get { return _rightField; }
            set
            {
                _rightField = value;
                this.ModelEntity.RightField = (_rightField != null) ? _rightField.GetModelEntity() : null;
                RaisePropertyChanged(() => this.RightField);
            }
        }
        private TransformationFieldViewModel _rightField;

        #endregion

        #region Validation

        public Boolean HasErrors
        {
            get
            {
                return (_leftField == null) ||
                       (String.IsNullOrWhiteSpace(_leftField.Name)) ||
                       (_leftField.Position < 0) ||
                       (_rightField == null) ||
                       (String.IsNullOrWhiteSpace(_rightField.Name)) ||
                       (_rightField.Position < 0);
            }
        }

        #endregion
    }
}