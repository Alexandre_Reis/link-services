﻿using System;
using Link.Entity.Model;

namespace Link.ViewModel
{
    /// <summary>
    /// ViewModel for the TransformationField entity.
    /// </summary>
    public class TransformationFieldViewModel : AuditableEntityViewModel<TransformationField>
    {
        #region Construction

        public TransformationFieldViewModel(TransformationField modelEntity)
            : base(modelEntity)
        {
            //
        }

        #endregion

        #region Bindable Entity Properties

        public String Name
        {
            get { return this.ModelEntity.Name; }
            set
            {
                if (this.ModelEntity.Name != value)
                {
                    this.ModelEntity.Name = value;
                    RaisePropertyChanged(() => this.Name);
                }
            }
        }

        public Int32 Position
        {
            get { return this.ModelEntity.Position; }
            set
            {
                if (this.ModelEntity.Position != value)
                {
                    this.ModelEntity.Position = value;
                    RaisePropertyChanged(() => this.Position);
                }
            }
        }

        /// <summary>
        /// A utility property indicating whether the user has selected the fied through the UI.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is user selected]; otherwise, <c>false</c>.
        /// </value>
        public Boolean IsUserSelected
        {
            get { return _isUserSelected; }
            set
            {
                if (_isUserSelected != value)
                {
                    _isUserSelected = value;
                    RaisePropertyChanged(() => this.IsUserSelected);
                }
            }
        }
        private Boolean _isUserSelected = false;

        /// <summary>
        /// A utility property indicating whether the field has been referenced by another class (like ReconKey or ReconComp)
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is field used]; otherwise, <c>false</c>.
        /// </value>
        public Boolean IsFieldUsed 
        {
            get { return _isFieldUsed; }
            set
            {
                if (_isFieldUsed != value)
                {
                    _isFieldUsed = value;
                    RaisePropertyChanged(() => this.IsFieldUsed);
                }
            }
        }
        private Boolean _isFieldUsed = false;

        #endregion
    }
}