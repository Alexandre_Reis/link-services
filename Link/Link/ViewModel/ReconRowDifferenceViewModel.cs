﻿using System;
using Link.Entity.Model;

namespace Link.ViewModel
{
    public class ReconRowDifferenceViewModel
    {
        #region Constructors

        public ReconRowDifferenceViewModel(ReconRowDifference entity)
        {
            _entity = entity;
        }

        #endregion

        #region Fields

        private ReconRowDifference _entity;

        #endregion

        #region Binding Properties

        public String LeftValue { get; set; }
        public String RightValue { get; set; }
        public String Key { get; set; }
        public Int32 LeftSourceRowIndex { get; set; }
        public Int32 RightSourceRowIndex { get; set; }

        #endregion
    }
}