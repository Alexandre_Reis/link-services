﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Link.ViewModel
{
    public class EntityValidationResult
    {
        #region Properties

        internal Dictionary<String, List<LocalizedString>> ResultSet
        {
            get { return _resultSet; }
        }
        private Dictionary<String, List<LocalizedString>> _resultSet = new Dictionary<String, List<LocalizedString>>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Adds a validation error to the validation result set.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyExpression">The property expression.</param>
        /// <param name="errorString">The error string.</param>
        /// <exception cref="System.Exception">propertyExpression must be a lambda expression that returns a property</exception>
        public void Add<T>(Expression<Func<T>> propertyExpression, LocalizedString errorString)
        {
            if ((propertyExpression.NodeType != ExpressionType.Lambda) || (propertyExpression.Body.NodeType != ExpressionType.MemberAccess))
                throw new Exception("propertyExpression must be a lambda expression that returns a property");

            Add(((MemberExpression)propertyExpression.Body).Member.Name, errorString);
        }

        /// <summary>
        /// Adds a validation error to the validation result set.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="errorString">The error string.</param>
        public void Add(String propertyName, LocalizedString errorString)
        {
            List<LocalizedString> errorList;

            if (_resultSet.TryGetValue(propertyName, out errorList) == false)
            {
                errorList = new List<LocalizedString>();
                _resultSet.Add(propertyName, errorList);
            }

            errorList.Add(errorString);
        }

        #endregion
    }
}
