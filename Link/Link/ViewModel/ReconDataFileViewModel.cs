﻿using System;

namespace Link.ViewModel
{
    public class ReconDataFileViewModel
    {
        #region Constructors

        public ReconDataFileViewModel(String fileData)
        {
            _fileData = fileData;

            //TODO: parse into displayable structure
        }

        #endregion

        #region Fields

        private String _fileData;

        #endregion

        #region Binding Properties

        //TEMP...
        public String FileData { get { return _fileData; } }

        #endregion
    }
}