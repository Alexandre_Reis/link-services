﻿using System;

namespace Link.ViewModel
{
    public class ReconResultSummaryViewModel
    {
        public String SummaryType { get; set; }
        public Decimal SummaryValue { get; set; }
    }
}