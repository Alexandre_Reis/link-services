﻿using System;
using System.Configuration;
using System.IO;
using Link;
using Link.Entity.Model;
using Link.Model;
using System.Collections.Generic;

namespace AtlasLinkService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ReconService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ReconService.svc or ReconService.svc.cs at the Solution Explorer and start debugging.
    public class ReconService : IReconService
    {
        private ReconEndpoints _endpoints;
        private string _user;
        private string _password;

        private ReconService()
        {
            if (_endpoints == null)
            {
                _user = ConfigurationManager.AppSettings["User"];
                _password = ConfigurationManager.AppSettings["Password"];
                var hostName = ConfigurationManager.AppSettings["HostName"];
                var hostPort = ConfigurationManager.AppSettings["HostPort"];
                var isEncrypted = ConfigurationManager.AppSettings["IsEncrypted"];
                var pollingReadDelay = ConfigurationManager.AppSettings["PollingReadDelay"];
                var failOverRetryMins = ConfigurationManager.AppSettings["FailOverRetryMins"];
                var defaultServerConnectionTimeout = ConfigurationManager.AppSettings["DefaultServerConnectionTimeout"];
                var extendedServerConnectionTimeout = ConfigurationManager.AppSettings["ExtendedServerConnectionTimeout"];
                var logDir = ConfigurationManager.AppSettings["LogDir"];
                var xlsFolder = ConfigurationManager.AppSettings["XlsFolder"];

                if (string.IsNullOrEmpty(_user))
                    throw new Exception("Missing the User parameter into the web.config");
                if (string.IsNullOrEmpty(_password))
                    throw new Exception("Missing the Password parameter into the web.config");
                if (string.IsNullOrEmpty(hostName))
                    throw new Exception("Missing the HostName parameter into the web.config");
                if (string.IsNullOrEmpty(hostPort))
                    throw new Exception("Missing the HostPort parameter into the web.config");
                if (string.IsNullOrEmpty(isEncrypted))
                    throw new Exception("Missing the IsEncrypted parameter into the web.config");
                if (string.IsNullOrEmpty(pollingReadDelay))
                    throw new Exception("Missing the PollingReadDelay parameter into the web.config");
                if (string.IsNullOrEmpty(failOverRetryMins))
                    throw new Exception("Missing the FailOverRetryMins parameter into the web.config");
                if (string.IsNullOrEmpty(defaultServerConnectionTimeout))
                    throw new Exception("Missing the DefaultServerConnectionTimeout parameter into the web.config");
                if (string.IsNullOrEmpty(extendedServerConnectionTimeout))
                    throw new Exception("Missing the ExtendedServerConnectionTimeout parameter into the web.config");

                _endpoints = new ReconEndpoints(hostName, Convert.ToInt32(hostPort), Convert.ToBoolean(isEncrypted), Convert.ToInt32(failOverRetryMins), Convert.ToInt32(defaultServerConnectionTimeout), Convert.ToInt32(extendedServerConnectionTimeout), logDir, xlsFolder);

            }
        }

        public LoginResult Authenticate()
        {
            LoginResult result = _endpoints.Authenticate(_user, _password);
            return result;
        }

        public ReconciliationResultResponse GetReconResultPosition(string messageId)
        {
            Authenticate();
            var reconciliationResultResponse = _endpoints.GetReconResultPosition(messageId);
            _endpoints.Logoff();
            return reconciliationResultResponse;
        }

        public ReconRouteFileLocation GetReconRoutePosition()
        {
            Authenticate();
            var reconRouteFileLocation = _endpoints.GetReconRoutePosition();
            _endpoints.Logoff();
            return reconRouteFileLocation;
        }

        public ReconciliationMessageResponse GetReconMessagePosition()
        {
            Authenticate();
            var reconciliationMessage = _endpoints.GetReconMessagePosition();
            _endpoints.Logoff();
            return reconciliationMessage;
        }

        public LinkResponse TriggerReconRoutePosition()
        {
            Authenticate();
            
            var response = _endpoints.TriggerReconPosition();
            _endpoints.Logoff();

            return response;
        }

        public LinkResponse Upload(ReconciliationFilePost post)
        {
            var file = Convert.FromBase64String(post.StringStreamFile);

            System.IO.FileStream outFile;
            try
            {
                var filePath = Path.Combine(post.FilePath, post.FileName);

                outFile = new System.IO.FileStream(filePath,
                                           System.IO.FileMode.Create,
                                           System.IO.FileAccess.Write);
                outFile.Write(file, 0, file.Length);
                outFile.Close();

                return new LinkResponse
                       {
                           ResponseStatus = ResponseType.OK,
                           Text = string.Format("{0} has been saved" ,filePath)
                       };

            }
            catch (Exception ex)
            {
                var filePath = Path.Combine(post.FilePath, post.FileName);

                return new LinkResponse
                {
                    ResponseStatus = ResponseType.ERROR,
                    Text = string.Format("Error on save {0}: {1}", filePath, ex.Message)
                };
            }
        }
    }
}
