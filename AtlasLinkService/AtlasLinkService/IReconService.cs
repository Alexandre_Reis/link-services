﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Link.Entity.Model;
using Link.Model;

namespace AtlasLinkService
{
    [ServiceContract]
    public interface IReconService
    {
        [OperationContract]
        LoginResult Authenticate();

        [OperationContract]
        ReconRouteFileLocation GetReconRoutePosition();

        [OperationContract]
        ReconciliationMessageResponse GetReconMessagePosition();

        [OperationContract]
        LinkResponse TriggerReconRoutePosition();

        [OperationContract]
        ReconciliationResultResponse GetReconResultPosition(string messageId);

        [OperationContract]
        LinkResponse Upload(ReconciliationFilePost post);

    }
}
